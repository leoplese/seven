\babel@toc {croatian}{}
\contentsline {chapter}{\numberline {1}Dnevnik promjena dokumentacije}{3}{chapter.1}% 
\contentsline {chapter}{\numberline {2}Opis projektnog zadatka}{7}{chapter.2}% 
\contentsline {chapter}{\numberline {3}Specifikacija programske potpore}{15}{chapter.3}% 
\contentsline {section}{\numberline {3.1}Funkcionalni zahtjevi}{15}{section.3.1}% 
\contentsline {subsection}{\numberline {3.1.1}Obrasci uporabe}{20}{subsection.3.1.1}% 
\contentsline {subsubsection}{Opis obrazaca uporabe}{20}{subsubsection*.2}% 
\contentsline {subsubsection}{Dijagrami obrazaca uporabe}{37}{subsubsection*.3}% 
\contentsline {subsection}{\numberline {3.1.2}Sekvencijski dijagrami}{40}{subsection.3.1.2}% 
\contentsline {section}{\numberline {3.2}Ostali zahtjevi}{44}{section.3.2}% 
\contentsline {chapter}{\numberline {4}Arhitektura i dizajn sustava}{46}{chapter.4}% 
\contentsline {section}{\numberline {4.1}Baza podataka}{51}{section.4.1}% 
\contentsline {subsection}{\numberline {4.1.1}Opis tablica}{51}{subsection.4.1.1}% 
\contentsline {subsection}{\numberline {4.1.2}Dijagram baze podataka}{59}{subsection.4.1.2}% 
\contentsline {section}{\numberline {4.2}Dijagram razreda}{60}{section.4.2}% 
\contentsline {section}{\numberline {4.3}Dijagram stanja}{72}{section.4.3}% 
\contentsline {section}{\numberline {4.4}Dijagram aktivnosti}{73}{section.4.4}% 
\contentsline {section}{\numberline {4.5}Dijagram komponenti}{74}{section.4.5}% 
\contentsline {chapter}{\numberline {5}Implementacija i korisničko sučelje}{75}{chapter.5}% 
\contentsline {section}{\numberline {5.1}Korištene tehnologije i alati}{75}{section.5.1}% 
\contentsline {section}{\numberline {5.2}Ispitivanje programskog rješenja}{77}{section.5.2}% 
\contentsline {subsection}{\numberline {5.2.1}Ispitivanje komponenti}{77}{subsection.5.2.1}% 
\contentsline {subsubsection}{Ispitni slučaj 1: Registracija korisnika s ispravnim podacima}{77}{subsubsection*.4}% 
\contentsline {subsubsection}{Ulaz:}{77}{subsubsection*.5}% 
\contentsline {subsubsection}{Očekivani rezultat:}{77}{subsubsection*.6}% 
\contentsline {subsubsection}{Rezultat:}{77}{subsubsection*.7}% 
\contentsline {subsubsection}{Ispitni slučaj 2: Registracija korisnika s neispravnim podacima}{77}{subsubsection*.8}% 
\contentsline {subsubsection}{Ulaz:}{77}{subsubsection*.9}% 
\contentsline {subsubsection}{Očekivani rezultat:}{78}{subsubsection*.10}% 
\contentsline {subsubsection}{Rezultat:}{78}{subsubsection*.11}% 
\contentsline {subsubsection}{Ispitni slučaj 3: Brisanje korisničkog računa koji postoji u bazi}{78}{subsubsection*.12}% 
\contentsline {subsubsection}{Ulaz:}{78}{subsubsection*.13}% 
\contentsline {subsubsection}{Očekivani rezultat:}{78}{subsubsection*.14}% 
\contentsline {subsubsection}{Rezultat:}{78}{subsubsection*.15}% 
\contentsline {subsubsection}{Ispitni slučaj 4: Brisanje korisničkog računa koji ne postoji u bazi}{78}{subsubsection*.16}% 
\contentsline {subsubsection}{Ulaz:}{78}{subsubsection*.17}% 
\contentsline {subsubsection}{Očekivani rezultat:}{78}{subsubsection*.18}% 
\contentsline {subsubsection}{Rezultat:}{78}{subsubsection*.19}% 
\contentsline {subsubsection}{Ispitni slučaj 5: Registracija romobila s ispravnim podacima}{79}{subsubsection*.20}% 
\contentsline {subsubsection}{Ulaz:}{79}{subsubsection*.21}% 
\contentsline {subsubsection}{Očekivani rezultat:}{79}{subsubsection*.22}% 
\contentsline {subsubsection}{Rezultat:}{79}{subsubsection*.23}% 
\contentsline {subsubsection}{Ispitni slučaj 6: Registracija romobila s neispravnim podacima}{79}{subsubsection*.24}% 
\contentsline {subsubsection}{Ulaz:}{79}{subsubsection*.25}% 
\contentsline {subsubsection}{Očekivani rezultat:}{79}{subsubsection*.26}% 
\contentsline {subsubsection}{Rezultat:}{79}{subsubsection*.27}% 
\contentsline {section}{\numberline {5.3}Dijagram razmještaja}{80}{section.5.3}% 
\contentsline {section}{\numberline {5.4}Upute za puštanje u pogon}{81}{section.5.4}% 
\contentsline {chapter}{\numberline {6}Zaključak i budući rad}{82}{chapter.6}% 
\contentsline {chapter}{Popis literature}{84}{chapter*.28}% 
\contentsline {chapter}{Indeks slika i dijagrama}{86}{chapter*.29}% 
\contentsline {chapter}{Dodatak: Prikaz aktivnosti grupe}{87}{chapter*.30}% 
