package com.seven.iznajmiromobil.view

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import java.io.ByteArrayOutputStream
import java.util.*

class BitmapBase64StringConverter {

    companion object {
        fun getBase64StringFromBitmap(bitmap: Bitmap): String {

            val TARGET_HEIGHT = 400.0
            var scaleFactor: Double = 1.0

            val matrix: Matrix = Matrix()
            if (bitmap.height > bitmap.width) {
                matrix.postRotate(270.0f)


                if (bitmap.width > TARGET_HEIGHT)
                    scaleFactor = TARGET_HEIGHT / bitmap.width


            } else {

                if (bitmap.height > TARGET_HEIGHT)
                    scaleFactor = TARGET_HEIGHT / bitmap.height
            }



            var scaledBitmap = Bitmap.createScaledBitmap(bitmap,(bitmap.getWidth()*scaleFactor).toInt(), (bitmap.getHeight()*scaleFactor).toInt(), true)
            var bitmapToConvert = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.width, scaledBitmap.height, matrix, true)
            val outputStream = ByteArrayOutputStream()
            bitmapToConvert.compress(Bitmap.CompressFormat.JPEG, 50, outputStream)
            val byteArray: ByteArray = outputStream.toByteArray()

            return Base64.getEncoder().encodeToString(byteArray)
        }

        fun getBitmapFromBase64String(base64String: String): Bitmap {

            val byteArray = Base64.getDecoder().decode(base64String)
            return BitmapFactory.decodeByteArray(byteArray, 0, byteArray.size, BitmapFactory.Options())
        }
    }
}