package com.seven.iznajmiromobil.di.modules

import android.app.Application
import com.seven.iznajmiromobil.IznajmiRomobilApplication
import com.seven.iznajmiromobil.di.scopes.ApplicationScope
import dagger.Binds
import dagger.Module
import dagger.android.support.AndroidSupportInjectionModule

@Module(
    includes = [
        AndroidSupportInjectionModule::class,
        ActivityModule::class,
        FragmentModule::class,
        ViewModelModule::class,
        ApiModule::class,
        RepositoryModule::class,
        ClientModule::class]
)
abstract class ApplicationModule {

    @Binds
    @ApplicationScope
    abstract fun bindApplication(application: IznajmiRomobilApplication): Application

}