package com.seven.iznajmiromobil.viewmodel

import androidx.lifecycle.MutableLiveData
import com.seven.iznajmiromobil.models.*
import com.seven.iznajmiromobil.network.api.ClientApi
import com.seven.iznajmiromobil.network.api.RenterApi
import com.seven.iznajmiromobil.network.api.UserApi
import com.seven.iznajmiromobil.network.repository.UserRepository
import com.seven.iznajmiromobil.view.base.BaseViewModel
import java.sql.Timestamp
import javax.inject.Inject

class UserViewModel @Inject constructor(private val userRepository: UserRepository) : BaseViewModel() {

    val loginResult = MutableLiveData<User>()
    val registerResult = MutableLiveData<User>()
    val resetPasswordResult = MutableLiveData<User>()
    val setPasswordResult = MutableLiveData<User>()

    var deleteUserResult = MutableLiveData<Unit>()
    var getAccountDataResult = MutableLiveData<UserApi.AccountDataResponseBody>()
    var changeAccountDataResult = MutableLiveData<User>()
    //var getAccountDataVisibility = MutableLiveData<ShowUserData>()
    //var changeAccountDataVisibilityResult = MutableLiveData<ShowUserData>()
    var getUserTransactionsResult = MutableLiveData<List<Transaction>>()
    var confirmTransactionResult = MutableLiveData<Message>()
    var changeUserRolesResult = MutableLiveData<User>()
    var getUserPublicProfileResult = MutableLiveData<UserPublicProfile>()
    var getUserRatingsResult = MutableLiveData<UserRatings>()
    var getCurrentOffersResult = MutableLiveData<List<AdvertisedCurrentRentOffer>>()
    var sendMessageInChatResult = MutableLiveData<Message>()
    var getMessagesInChatResult = MutableLiveData<List<Message>>()

    var getRenterOffersResult = MutableLiveData<List<RenterRentOffer>>()
    var getClientOffersResult = MutableLiveData<List<RenterRentOffer>>()
    var createOfferResult = MutableLiveData<Unit>()
    var createScooterResult = MutableLiveData<Scooter>()
    var deleteScooterResult = MutableLiveData<Unit>()
    var getRenterScootersResult = MutableLiveData<List<Scooter>>()
    var addScooterPhotosResult = MutableLiveData<Scooter>()
    var deleteScooterPhotosResult = MutableLiveData<Scooter>()
    var getTransactionResult  = MutableLiveData<BasicTransaction>()
    var createRatingResult = MutableLiveData<Unit>()
    var getClientRequestsResult = MutableLiveData<List<ClientRequest>>()
    var confirmClientRequestForOfferResult = MutableLiveData<Message>()
    var getClientReportsResult = MutableLiveData<ArrayList<ClientReport>>()
    var answerClientReportForOfferResult = MutableLiveData<Message>()
    var getChatsResult = MutableLiveData<List<Chat>>()
    var sendRequestForOfferResult = MutableLiveData<Message>()
    var sendReportForOfferResult = MutableLiveData<Message>()
    var offerDetailsResult = MutableLiveData<UserApi.OfferDetails>()



    fun login(email: String, password: String) {
        executeTask(userRepository.login(UserApi.LoginRequestBody(email, password)), { data ->
            val response = data as UserApi.UserResponseBody
            loginResult.value = response.user
        })
    }

    fun register(name: String, surname: String, username: String, email: String, creditCardNumber: String, isRenter: Boolean, isClient: Boolean, idString: String, certificateString: String) {
        executeTask(userRepository.register(UserApi.RegisterRequestBody(name, surname, username, creditCardNumber, email, isRenter, isClient, idString, certificateString)), { data ->
            val response = data as UserApi.UserResponseBody
            registerResult.value = response.user
        })
    }

    fun resetPassword(email: String, oldPassword: String, newPassword: String) {
        executeTask(userRepository.resetPassword(UserApi.ResetPasswordRequestBody(email, oldPassword, newPassword)), { data ->
            val response = data as UserApi.UserResponseBody
            resetPasswordResult.value = response.user
        })
    }

    fun setPassword(email: String, password: String) {
        executeTask(userRepository.setPassword(UserApi.LoginRequestBody(email, password)), { data ->
            val response = data as UserApi.UserResponseBody
            setPasswordResult.value = response.user
        })
    }





    fun deleteUser(email: String) {
        executeTask(userRepository.deleteUser(UserApi.EmailUserBody(email)), { data ->
            deleteUserResult.value = data as Unit
        })
    }

    fun getAccountData(email: String) {
        executeTask(userRepository.getAccountData(UserApi.EmailUserBody(email)), { data ->
            val response = data as UserApi.AccountDataResponseBody
            getAccountDataResult.value = response
        })
    }

    /*
    fun changeAccountData(name: String, surname: String, nickname: String, email: String, creditCardNumber: String) {
        executeTask(userRepository.changeAccountData(UserApi.UserAccountDataBody(name, surname, nickname, email, creditCardNumber)), { data ->
            val response = data as UserApi.UserResponseBody
            changeAccountDataResult.value = response.user
        })
    }
    */

/*
    fun getAccountDataVisibility(email: String) {
        executeTask(userRepository.getAccountDataVisibility(UserApi.EmailUserBody(email)), { data ->
            val response = data as UserApi.ShowUserDataResponseBody
            getAccountDataVisibility.value = response.showUserData
        })
    }
*/


    /*
    fun changeAccountDataVisibility(email: String, showName: Boolean, showSurname: Boolean, showEmail: Boolean) {
        executeTask(userRepository.changeAccountDataVisibility(UserApi.UserAccountDataVisibilityBody(email, showName, showSurname, showEmail)), { data ->
            val response = data as UserApi.ShowUserDataResponseBody
            changeAccountDataVisibilityResult.value = response.showUserData
        })
    }
     */

    fun changeAccountData(name: String, surname: String, nickname: String, email: String, creditCardNumber: String, showName: Boolean, showSurname: Boolean, showEmail: Boolean) {
        executeTask(userRepository.changeAccountData(UserApi.UserAccountDataAndDataVisibilityBody(name, surname, nickname, email, creditCardNumber, showName, showSurname, showEmail)), { data ->
            val response = data as UserApi.UserResponseBody
            changeAccountDataResult.value = response.user
        })
    }

    fun getUserTransactions(email: String) {
        executeTask(userRepository.getUserTransactions(UserApi.EmailUserBody(email)), { data ->
            val response = data as UserApi.TransactionResponseBody
            getUserTransactionsResult.value = response.transactions
        })
    }

    fun changeUserRoles(email: String, isRenter: Boolean, isClient: Boolean) {
        executeTask(userRepository.changeUserRoles(UserApi.UserRolesBody(email, isRenter, isClient)), { data ->
            val response = data as UserApi.UserResponseBody
            changeUserRolesResult.value = response.user
        })
    }

    fun getUserPublicProfile(nickname: String) {
        executeTask(userRepository.getUserPublicProfile(UserApi.NicknameUserBody(nickname)), { data ->
            val response = data as UserApi.UserPublicProfileResponseBody
            getUserPublicProfileResult.value = response.userPublicProfile
        })
    }

    fun getUserRatings(email: String) {
        executeTask(userRepository.getUserRatings(UserApi.EmailUserBody(email)), { data ->
            val response = data as UserApi.UserRatingsResponseBody
            getUserRatingsResult.value = response.userRatings
        })
    }

    // TODO implement in activities
    fun getCurrentOffers(timestamp: String) {
        executeTask(userRepository.getCurrentOffers(RenterApi.StringTimeStampBody(timestamp)), { data ->
            val response = data as UserApi.CurrentOffersResponseBody
            getCurrentOffersResult.value = response.rentOffer
        })
    }

    // TODO implement in activities
    fun sendMessageInChat(chatId: Int, text: String, senderId: Int) {
        executeTask(userRepository.sendMessageInChat(UserApi.SendMessageInChatRequestBody(chatId, text)), { data ->
            val response = data as UserApi.SendMessageInChatResponseBody
            val message = Message()
            message.text = text
            message.time = response.time
            message.senderId = senderId
            sendMessageInChatResult.value = message
        })
    }

    // TODO implement in activities
    fun getMessagesInChat(chatId: Int) {
        executeTask(userRepository.getMessagesInChat(UserApi.GetMessagesInChatRequestBody(chatId)), { data ->
            val response = data as UserApi.GetMessagesInChatResponseBody
            getMessagesInChatResult.value = response.messages
        })
    }

    // TODO implement in activities
    fun getRenterOffers(email: String) {
        executeTask(userRepository.getRenterOffers(UserApi.EmailUserBody(email)), { data ->
            val response = data as UserApi.OffersResponseBody
            getRenterOffersResult.value = response.rentOffer
        })
    }

    // TODO implement in activities
    fun getClientOffers(email: String) {
        executeTask(userRepository.getClientOffers(UserApi.EmailUserBody(email)), { data ->
            val response = data as UserApi.OffersResponseBody
            getClientOffersResult.value = response.rentOffer
        })
    }

    // TODO implement in activities
    fun createOffer(scooterId: Int,
                    currentLocationLatitude: Double,
                    currentLocationLongitude: Double,
                    returnLocationLatitude: Double,
                    returnLocationLongitude: Double,
                    returnTime: Timestamp,
                    priceKm: Double,
                    returnPenalty: Double) {
        executeTask(userRepository.createOffer(RenterApi.CreateOfferBody(scooterId, currentLocationLatitude, currentLocationLongitude, returnLocationLatitude, returnLocationLongitude, returnTime, priceKm, returnPenalty)), { data ->
            createOfferResult.value = data as Unit
        })
    }

    // TODO implement in activities
    fun createScooter(renterId: Int, comment: String, isAvailable: Boolean) {
        executeTask(userRepository.createScooter(RenterApi.CreateScooterBody(renterId, comment, isAvailable)), { data ->
            val response = data as RenterApi.ScooterResponseBody
            createScooterResult.value = response.scooter
        })
    }

    // TODO implement in activities
    fun deleteScooter(scooterId: Int) {
        executeTask(userRepository.deleteScooter(RenterApi.IDScooterBody(scooterId)), { data ->
            deleteScooterResult.value = data as Unit
        })
    }

    // TODO implement in activities
    fun getRenterScooters(email: String) {
        executeTask(userRepository.getRenterScooters(UserApi.EmailUserBody(email)), { data ->
            val response = data as RenterApi.ScootersResponseBody
            getRenterScootersResult.value = response.scooters
        })
    }

    // TODO implement in activities
    fun addScooterPhotos(scooterId: Int, base64Image: String) {
        executeTask(userRepository.addScooterPhotos(RenterApi.AddOrDeleteScooterPhotosBody(scooterId, base64Image)), { data ->
            val response = data as RenterApi.ScooterResponseBody
            addScooterPhotosResult.value = response.scooter
        })
    }

    // TODO implement in activities
    fun deleteScooterPhotos(scooterId: Int, base64Image: String) {
        executeTask(userRepository.deleteScooterPhotos(RenterApi.AddOrDeleteScooterPhotosBody(scooterId, base64Image)), { data ->
            val response = data as RenterApi.ScooterResponseBody
            deleteScooterPhotosResult.value = response.scooter
        })
    }

    // TODO implement in activities
    fun getTransaction(rentOfferId: Int) {
        executeTask(userRepository.getTransaction(RenterApi.GetTransactionRequestBody(rentOfferId)), { data ->
            val response = data as RenterApi.GetTransactionResponseBody
            getTransactionResult.value = response.getTransactionData
        })
    }

    fun confirmTransaction(basicTransaction: BasicTransaction) {
        executeTask(userRepository.confirmTransaction(RenterApi.ConfirmTransactionRequestBody(basicTransaction)), { data ->
            val response = data as RenterApi.ConfirmTransactionResponseBody
            confirmTransactionResult.value = response.confirmMessage
        })
    }

    fun createRating(rentOfferId: Int, rating: Int, comment: String) {
        executeTask(userRepository.createRating(RenterApi.CreateRatingRequestBody(rentOfferId, rating, comment)), { data ->
            createRatingResult.value = data as Unit
        })
    }

    // TODO implement in activities
    fun getClientRequests(rentOfferId: Int) {
        executeTask(userRepository.getClientRequests(RenterApi.GetClientRequestsRequestBody(rentOfferId)), { data ->
            val response = data as RenterApi.GetClientRequestsResponseBody
            getClientRequestsResult.value = response.clientRequests
        })
    }

    fun confirmClientRequestForOffer(chatId: Int, rentOfferId: Int) {
        executeTask(userRepository.confirmClientRequestForOffer(RenterApi.ConfirmClientRequestForOfferRequestBody(chatId, rentOfferId)), { data ->
            val response = data as RenterApi.ConfirmClientRequestForOfferResponseBody
            confirmClientRequestForOfferResult.value = response.confirmMessage
        })
    }

    fun getClientReports(email: String) {
        executeTask(userRepository.getClientReports(UserApi.EmailUserBody(email)), { data ->
            val response = data as RenterApi.GetClientReportsReponseBody
            getClientReportsResult.value = response.clientReports
        })
    }

    fun answerClientReportForOffer(rentOfferId: Int, answerYesNo: Boolean) {
        executeTask(userRepository.answerClientReportForOffer(RenterApi.AnswerClientReportForOfferRequestBody(rentOfferId, answerYesNo)), { data ->
            val response = data as RenterApi.AnswerClientReportForOfferResponseBody
            answerClientReportForOfferResult.value = response.acceptDenyMessage
        })
    }

    fun getChats(email: String) {
        executeTask(userRepository.getChats(UserApi.EmailUserBody(email)), { data ->
            val response = data as UserApi.GetChatsResponseBody
            getChatsResult.value = response.chats
        })
    }

    fun sendRequestForOffer(senderId: Int, rentOfferId: Int, text: String) {
        executeTask(userRepository.sendRequestForOffer(ClientApi.SendRequestForOfferRequestBody(senderId, rentOfferId, text)), { data ->
            val response = data as ClientApi.SendRequestForOfferResponseBody
            sendRequestForOfferResult.value = response.requestMessage
        })
    }

    // TODO implement in activities
    fun sendReportForOffer(rentOfferId: Int,
                           cause: String,
                           photosToAdd: List<ScooterPhoto>,
                           photosToRemove: List<ScooterPhoto>) {
        executeTask(userRepository.sendReportForOffer(ClientApi.SendReportForOfferRequestBody(rentOfferId, cause, photosToAdd, photosToRemove)), { data ->
            val response = data as ClientApi.SendReportForOfferResponseBody
            sendReportForOfferResult.value = response.reportMessage
        })
    }

    fun getOfferDetails(offerId: Int) {
        executeTask(userRepository.getOfferDetails(UserApi.OfferDetailsBody(offerId)), { data ->
            val response = data as UserApi.OfferDetails
            offerDetailsResult.value = response
        })
    }

}