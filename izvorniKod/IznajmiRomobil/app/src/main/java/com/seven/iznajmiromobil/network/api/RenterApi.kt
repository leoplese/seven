package com.seven.iznajmiromobil.network.api

import com.seven.iznajmiromobil.models.*
import io.reactivex.Observable
import retrofit2.http.*
import java.sql.Timestamp

interface RenterApi {



    data class CreateOfferBody(
        var scooterId: Int, // renter 1. chooses "Choose scooter for offer", 2. chooses one scooter -> save scooterId, 3. use that scooterId to create new offer for it HERE
        var currentLocationLatitude: Double,
        var currentLocationLongitude: Double,
        var returnLocationLatitude: Double,
        var returnLocationLongitude: Double,
        var returnTime: Timestamp,
        var priceKm: Double,
        var returnPenalty: Double
    )

    data class CreateScooterBody(
        var renterId: Int,
        var comment: String,
        var isAvailable: Boolean
    )

    data class IDScooterBody(var id: Int)

    data class ScootersResponseBody(var scooters: List<Scooter>)

    data class ScooterResponseBody(var scooter: Scooter)

    data class AddOrDeleteScooterPhotosBody(
        var scooterId: Int,
        var imgUrl: String
    )

    data class GetTransactionRequestBody(
        var rentOfferId: Int // renter 1. chooses "My offers", 2. chooses one offer -> save rentOfferId, 3. use that rentOfferId to create new transaction for it HERE
    )

    data class GetTransactionResponseBody(
        var getTransactionData: BasicTransaction
    )

    data class ConfirmTransactionRequestBody(
        var confirmTransactionData: BasicTransaction
    )

    data class ConfirmTransactionResponseBody(
        var confirmMessage: Message
    )

    data class CreateRatingRequestBody(
        var rentOfferId: Int, // renter 1. chooses "My offers", 2. chooses one offer -> save rentOfferId, 3. use that rentOfferId to create new rating for client associated with it HERE
        var rating: Int,
        var comment: String
    )

    data class GetClientRequestsRequestBody(
        var rentOfferId: Int
    )

    data class GetClientRequestsResponseBody(
        var clientRequests: List<ClientRequest>
    )

    data class ConfirmClientRequestForOfferRequestBody(
        var chatId: Int,    // chat to which the CONFIRMED request message belongs to -> backend uses it to send positive answer to the request in this chat (both to client and renter specified in it)
        var rentOfferId: Int    // used to send DENIED request message to all the other chats related to the offer EXCEPT to the one that has "chatId" in line above
    )

    data class ConfirmClientRequestForOfferResponseBody(
        var confirmMessage: Message
    )

    data class GetClientReportsReponseBody(var clientReports: ArrayList<ClientReport>)

    data class AnswerClientReportForOfferRequestBody(
        var rentOfferId: Int,  // renter 1. chooses "Client reports", 2. chooses one report -> save rentOfferId, 3. use that rentOfferId to send answer to the report associated with it HERE
        var answerYesNo: Boolean
    )

    data class AnswerClientReportForOfferResponseBody(
        var acceptDenyMessage: Message
    )

    data class TimeStampBody(var dateTime: Timestamp)

    data class StringTimeStampBody(var dateTime: String)

    /**
     * UC 17
     * @param user (renter) to get his offers - identified by email
     * @return renter's rent offers - chronologically
     */
    @POST("get_rent_offers_renter/")
    fun getRenterOffers(@Body emailUserBody: UserApi.EmailUserBody): Observable<UserApi.OffersResponseBody>

    /**
     * UC 18
     * @param new offer data
     * @return nothing (Unit)
     */
    @POST("create_rentOffer/")
    fun createOffer(@Body createOfferBody: CreateOfferBody): Observable<Unit>


    /**
     * UC 12
     * @param new scooter data
     * @return nothing (Unit)
     */
    @POST("create_scooter/")
    fun createScooter(@Body createScooterBody: CreateScooterBody): Observable<ScooterResponseBody>

    /**
     * UC 13
     * @param scooter - identified by scooter ID
     * @return nothing (Unit)
     */
    @POST("delete_scooter/")
    fun deleteScooter(@Body idScooterBody: IDScooterBody): Observable<Unit>

    /**
     * UC 14
     * @param user (renter) to get his scooters - identified by email
     * @return renter's scooters
     */
    @POST("get_scooters/")
    fun getRenterScooters(@Body emailUserBody: UserApi.EmailUserBody): Observable<ScootersResponseBody>

    /**
     * UC 15
     * @param user (renter) identified by email with new renter/client role
     * @return User data
     */
    @POST("add_scooter_photo/")
    fun addScooterPhoto(@Body addScooterPhotosBody: AddOrDeleteScooterPhotosBody): Observable<ScooterResponseBody>

    /**
     * UC 16
     * @param user (renter) identified by email with new renter/client role
     * @return User data
     */
    @PUT("delete_scooter_photos/")
    fun deleteScooterPhotos(@Body deleteScooterPhotosBody: AddOrDeleteScooterPhotosBody): Observable<ScooterResponseBody>


    /**
     * UC 25 - part 1 (see + part 2 below)
     * @param rent offer ID for which transaction is being created
     * @return new created transaction for rent offer (data gotten and generated - everything in backend, BUT not stored in the database YET!)
     */
    @GET("get_transaction/")
    fun getTransaction(@Body getTransactionBody: GetTransactionRequestBody): Observable<GetTransactionResponseBody>

    /**
     * UC 25 - part 2 (see + part 1 above)
     * @param transaction data of the transaction which is to be confirmed (and stored in the database NOW)
     * @return time when exactly the transaction was confirmed - time when the "Transaction successful" message is sent in the chat
     */
    @POST("confirm_transaction/")
    fun confirmTransaction(@Body confirmTransactionRequestBody: ConfirmTransactionRequestBody): Observable<ConfirmTransactionResponseBody>

    /**
     * UC 26
     * @param rent offer ID for which rating is being created
     * @return nothing (Unit)
     */
    @POST("create_rating/")
    fun createRating(@Body createRatingRequestBody: CreateRatingRequestBody): Observable<Unit>


    /**
     * UC 19
     * @param rent offer ID whose client requests are to be retrieved
     * @return client requests for the offer
     */
    @GET("get_client_requests/")
    fun getClientRequests(@Body getClientRequestsRequestBody: GetClientRequestsRequestBody): Observable<GetClientRequestsResponseBody>

    /**
     * UC 20
     * @param rent offer and chat for the offer to send positive/negative answers to
     * @return time when exactly the confirmation message of client request message was sent - little feedback to the renter (could be nothing (Unit))
     */
    @POST("confirm_client_request/")
    fun confirmClientRequestForOffer(@Body confirmClientRequestForOfferRequestBody: ConfirmClientRequestForOfferRequestBody): Observable<ConfirmClientRequestForOfferResponseBody>


    /**
     * @param renter whose client reports for all his active rent offers must be retrieved - identified by email
     * @return list of client reports
     */
    @POST("get_client_reports/")
    fun getClientReports(@Body emailUserBody: UserApi.EmailUserBody): Observable<GetClientReportsReponseBody>

    /**
     * UC 21
     * @param rent offer to which the report belongs + answer YES/NO to the report
     * @return time when exactly the response message to report message was sent - little feedback to the renter (could be nothing (Unit))
     */
    @POST("answer_client_report/")
    fun answerClientReportForOffer(@Body answerClientReportForOfferRequestBody: AnswerClientReportForOfferRequestBody): Observable<AnswerClientReportForOfferResponseBody>


    ///////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////// UserApi /////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////

    data class LoginRequestBody(var email: String, var password: String)
    data class ResetPasswordRequestBody(var email: String, var oldPassword: String, var newPassword: String)
    data class UserResponseBody(var user: User)

    data class IDUserBody(var userId: Int)
    data class EmailUserBody(var email: String)
    data class NicknameUserBody(var nickname: String)
    data class UserAccountDataAndDataVisibilityBody(var name: String, var surname: String, var nickname: String, var email: String, var creditCardNumber: String, var showName: Boolean, var showSurname: Boolean, var showEmail: Boolean)
    //data class UserAccountDataVisibilityBody(var email: String, var showName: Boolean, var showSurname: Boolean, var showEmail: Boolean)
    data class ShowUserDataResponseBody(var showUserData: ShowUserData)

    data class TransactionResponseBody(var transactions: List<Transaction>)
    data class UserRolesBody(var email: String, var isRenter: Boolean, var isClient: Boolean)
    data class UserPublicProfileResponseBody(var userPublicProfile: UserPublicProfile)
    data class UserRatingsResponseBody(var userRatings: UserRatings)

    data class CurrentOffersResponseBody(var currentOffers: List<AdvertisedCurrentRentOffer>)
    data class SendMessageInChatRequestBody(
        var chatId: Int,    // user 1. chooses "My chats", 2. chooses one chat -> save chatId, 3. use that chatId to create/send new message for it HERE
        var text: String
    )
    data class SendMessageInChatResponseBody(
        var time: Timestamp    // just to give user a feedback about time when exactly the message was sent (time written in the message)
    )

    data class GetChatsResponseBody(
        var chats: List<Chat>
    )

    data class GetMessagesInChatRequestBody(
        var chatId: Int     // user 1. chooses "My chats", 2. chooses one chat -> save chatId, 3. use that chatId to get messages in that chat
    )
    data class GetMessagesInChatResponseBody(
        var messages: List<Message>
    )

    data class RegisterRequestBody(
        var name: String,
        var surname: String,
        var nickname: String,
        var creditCardNumber: String,
        var email: String,
        var isRenter: Boolean,
        var isClient: Boolean
    )

    data class OffersResponseBody(var renterOffers: List<RenterRentOffer>)

    /**
     * UC 2
     * @param register user data
     * @return Unit (nothing to return)
     */
    @POST("register/")
    fun register(@Body registerBody: RegisterRequestBody): Observable<Unit>

    /**
     * UC 3
     * @param login user data
     * @return user data
     */
    @POST("login/")
    fun login(@Body loginBody: LoginRequestBody): Observable<UserResponseBody>

    /**
     * Called when user changes password.
     */
    @PUT("reset_password/")
    fun resetPassword(@Body resetPasswordBody: UserApi.ResetPasswordRequestBody): Observable<UserApi.UserResponseBody>

    // called on deep link given from email
    @PUT("set_password/")
    fun setPassword(@Body resetPasswordBody: UserApi.LoginRequestBody): Observable<UserApi.UserResponseBody>


    /**
     * UC 4
     * @param user to delete - identified by email
     * @return Unit (nothing to return)
     */
    @DELETE("delete_user/")
    fun deleteUser(@Body deleteUserBody: EmailUserBody): Observable<Unit>

    /**
     * UC 5
     * @param user to get his account data - identified by email
     * @return User data
     */
    @GET("account_data/")
    fun getAccountData(@Body getAccountDataBody: EmailUserBody): Observable<UserResponseBody>

    /**
     * UC 6
     * @param user data (name, surname, nickname, email, creditCardNumber) to update
     * @return User data
     */
    //@PUT("update_info/")
    //fun changeAccountData(@Body changeAccountDataBody: UserAccountDataBody): Observable<UserResponseBody>


    /**
     * @param user to get his account data - identified by email
     * @return User data visibility
     */
    /*
    @GET("account_data_visibility/")
    fun getAccountDataVisibility(@Body getAccountDataVisibilityBody: EmailUserBody): Observable<ShowUserDataResponseBody>
     */

    /**
     * UC 7
     * @param user data visibility (email, showName, showSurname, showEmail) to update
     * @return User data
     */
    //@PUT("change_account_data_visibility/")
    //fun changeAccountDataVisibility(@Body changeAccountDataVisibilityBody: UserAccountDataVisibilityBody): Observable<ShowUserDataResponseBody>

    /**
     * UC 6 + UC 7 merged into this one UC
     *
     * @param user data visibility (name, surname, nickname, email, creditCardNumber, showName, showSurname, showEmail) to update
     * @return User data
     */
    @PUT("update_info/")
    fun changeAccountData(@Body changeAccountDataAndDataVisibilityBody: UserAccountDataAndDataVisibilityBody): Observable<UserResponseBody>


    /**
     * UC 8
     * @param user to get his account data - identified by email
     * @return User transactions data
     */
    @GET("get_transactions/")
    fun getUserTransactions(@Body getUserTransactionsBody: EmailUserBody): Observable<TransactionResponseBody>

    /**
     * UC 9, UC 10
     * @param user identified by email with new renter/client role
     * @return User data
     */
    @PUT("change_roles/")
    fun changeUserRoles(@Body userRolesBody: UserRolesBody): Observable<UserResponseBody>


    /**
     * UC 11
     * @param user (any existing user) being searched in Search box - identified by email
     * @return User data
     */
    @GET("get_user_public_profile/")
    fun getUserPublicProfile(@Body nicknameUserBody: NicknameUserBody): Observable<UserPublicProfileResponseBody>

    /**
     *
     */
    @GET("get_user_ratings/")
    fun getUserRatings(@Body emailUserBody: EmailUserBody): Observable<UserRatingsResponseBody>


//    /**
//     * UC 1
//     *
//     * The first use case - any user (unregistered or registered (client or renter)) can do this - get current/actual/non-expired renter offers.
//     *
//     * @param nothing (get all the active i.e. current offers from all the renters)
//     * @return current offers
//     */
//    @POST("get_active_offers/")
//    fun getCurrentOffers(@Body timeStampBody: TimeStampBody ): Observable<CurrentOffersResponseBody>

    /**
     * UC 22
     * @param chat to which message is being sent + message text
     * @return time when exactly the message was sent - little feedback to the user (could be nothing (Unit))
     */
    @POST("send_message/")
    fun sendMessageInChat(@Body sendMessageInChatRequestBody: SendMessageInChatRequestBody): Observable<SendMessageInChatResponseBody>

    /**
     * UC 23
     *
     * This method is called by user with renter role - can be ONLY renter or renter+client.
     *
     * @param chat to which message is being sent + message text
     * @return time when exactly the message was sent - little feedback to the user (could be nothing (Unit))
     */
    @GET("get_chats/")
    fun getChats(@Body emailUserBody: EmailUserBody): Observable<GetChatsResponseBody>


    /**
     * UC 24
     * @param chat whose messages are being retrieve
     * @return messages in chat
     */
    @GET("get_chat_messages/")
    fun getMessagesInChat(@Body getMessagesInChatRequestBody: GetMessagesInChatRequestBody): Observable<GetMessagesInChatResponseBody>

}