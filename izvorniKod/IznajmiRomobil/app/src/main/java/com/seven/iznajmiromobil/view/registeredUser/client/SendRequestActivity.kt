package com.seven.iznajmiromobil.view.registeredUser.client

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.SharedPreferences
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.core.app.NavUtils
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.gson.Gson
import com.seven.iznajmiromobil.R
import com.seven.iznajmiromobil.databinding.SendRequestActivityBinding
import com.seven.iznajmiromobil.models.User
import com.seven.iznajmiromobil.view.base.BaseActivity
import com.seven.iznajmiromobil.viewmodel.UserViewModel
import javax.inject.Inject

class SendRequestActivity : BaseActivity()  {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: SendRequestActivityBinding

    private lateinit var userViewModel: UserViewModel

    private val SHARED_PREF_NAME = "userSharedPrefs"
    private lateinit var sharedPreferences: SharedPreferences

    private val USER_SHARED_PREF_KEY: String = "userData"

    private lateinit var user: User

    private var rentOfferId: Int = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.send_request_activity)
        binding.activity = this
        userViewModel = ViewModelProviders.of(this, viewModelFactory)[UserViewModel::class.java]

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        sharedPreferences = getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)

        subscribeUi()

        // get stored user data
        val json = sharedPreferences.getString(USER_SHARED_PREF_KEY, null)
        user  = Gson().fromJson(json, User::class.java)

        rentOfferId = intent.getIntExtra("rentOfferId", -1)

    }

    private fun subscribeUi() {
        userViewModel.sendRequestForOfferResult.observe(this, Observer{ message ->
            // following toast is just ot inform client request has been sent
            Toast.makeText(this, "Request sent!", Toast.LENGTH_SHORT).show()
        })

    }

    fun sendRequest() {
        showPopupConfirmSendRequest()

    }

    private fun showPopupConfirmSendRequest() {
        AlertDialog.Builder(this)
            .setCancelable(false)
            .setTitle(R.string.send_request)
            .setMessage(R.string.are_you_sure_send_request)
            .setPositiveButton(R.string.yes,
                DialogInterface.OnClickListener { dialogInterface, i ->
                    userViewModel.sendRequestForOffer(user.id, rentOfferId, binding.requestCommentInputEditText.text.toString())
                })
            .setNegativeButton(R.string.no,
                DialogInterface.OnClickListener { dialogInterface, i ->
                    dialogInterface.dismiss()
                })
            .show()
    }

    fun cancelRequest() {
        showPopupDiscardChanges("cancelBtn")
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> showPopupDiscardChanges("upBtn")
        }

        return true
    }


    override fun onBackPressed() {
        showPopupDiscardChanges("backBtn")
        return
    }

    private fun showPopupDiscardChanges(discardType: String) {

        AlertDialog.Builder(this)
            .setCancelable(false)
            .setTitle(R.string.cancel_request)
            .setMessage(R.string.are_you_sure_cancel_request)
            .setPositiveButton(R.string.yes,
                DialogInterface.OnClickListener { dialogInterface, i ->
                    when (discardType) {
                        "cancelBtn" -> NavUtils.navigateUpFromSameTask(this)
                        "upBtn" -> NavUtils.navigateUpFromSameTask(this)
                        "backBtn" -> super.onBackPressed()
                    }

                })
            .setNegativeButton(R.string.no,
                DialogInterface.OnClickListener { dialogInterface, i ->
                    dialogInterface.dismiss()
                })
            .show()
    }
}
