package com.seven.iznajmiromobil.di.modules

import com.seven.iznajmiromobil.di.scopes.ApplicationScope
import com.seven.iznajmiromobil.network.api.ClientApi
import com.seven.iznajmiromobil.network.api.RenterApi
import com.seven.iznajmiromobil.network.api.TransactionApi
import com.seven.iznajmiromobil.network.api.UserApi
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module(includes = [ClientModule::class])
class ApiModule {

    @Provides
    @ApplicationScope
    fun provideUserApi(retrofit: Retrofit): UserApi = retrofit.create(UserApi::class.java)

    @Provides
    @ApplicationScope
    fun provideTransactionApi(retrofit: Retrofit): TransactionApi = retrofit.create(TransactionApi::class.java)

    @Provides
    @ApplicationScope
    fun provideRenterApi(retrofit: Retrofit): RenterApi = retrofit.create(RenterApi::class.java)

    @Provides
    @ApplicationScope
    fun provideClientApi(retrofit: Retrofit): ClientApi = retrofit.create(ClientApi::class.java)


}