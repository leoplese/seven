package com.seven.iznajmiromobil.view.chat

import android.annotation.SuppressLint
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.seven.iznajmiromobil.R
import com.seven.iznajmiromobil.models.Message
import kotlinx.android.synthetic.main.message_view_holder.view.*
import java.text.SimpleDateFormat
import java.util.*

class MessagesAdapter(var messages: List<Message>) :
    RecyclerView.Adapter<MessagesAdapter.MessageViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessageViewHolder {
        return MessageViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.message_view_holder,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return messages.size
    }

    override fun onBindViewHolder(holder: MessageViewHolder, position: Int) {
        holder.bind(messages[position])
    }

    class MessageViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SimpleDateFormat")
        fun bind(message: Message) {
            itemView.message.text = message.text
            val sdf = SimpleDateFormat("HH:mm")
            val netDate = Date(message.time.time)
            itemView.timestamp.text = sdf.format(netDate)
            if (message.senderId != 0) itemView.root_layout.gravity = Gravity.END
            else itemView.root_layout.gravity = Gravity.START
        }
    }
}