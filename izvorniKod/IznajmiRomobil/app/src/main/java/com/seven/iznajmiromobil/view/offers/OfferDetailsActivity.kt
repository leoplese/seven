package com.seven.iznajmiromobil.view.offers

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.gson.Gson
import com.seven.iznajmiromobil.R
import com.seven.iznajmiromobil.databinding.OfferDetailsActivityBinding
import com.seven.iznajmiromobil.models.User
import com.seven.iznajmiromobil.network.api.UserApi
import com.seven.iznajmiromobil.view.BitmapBase64StringConverter
import com.seven.iznajmiromobil.view.base.BaseActivity
import com.seven.iznajmiromobil.view.scooters.ScooterPhotosAdapter
import com.seven.iznajmiromobil.viewmodel.UserViewModel
import java.text.SimpleDateFormat
import javax.inject.Inject


class OfferDetailsActivity : BaseActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: OfferDetailsActivityBinding
    private lateinit var viewModel: UserViewModel

    private lateinit var user: User
    private lateinit var offerDetails: UserApi.OfferDetails

    private val SHARED_PREF_NAME = "userSharedPrefs"
    private val USER_SHARED_PREF_KEY: String = "userData"

    private lateinit var scooterPhotosAdapter: ScooterPhotosAdapter
    private val scooterPhotos = mutableListOf<Bitmap>()

    companion object {
        fun buildIntent(context: Context, offerId: Int): Intent {
            val intent = Intent(context, OfferDetailsActivity::class.java)
            intent.putExtra("offerId", offerId)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.offer_details_activity)
        binding.activity = this
        viewModel = ViewModelProviders.of(this, viewModelFactory)[UserViewModel::class.java]
        subscribeUi()

        val sharedPreferences: SharedPreferences =
            this.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
        val json = sharedPreferences.getString(USER_SHARED_PREF_KEY, null)
        user = Gson().fromJson(json, User::class.java)

        viewModel.getOfferDetails(intent.getIntExtra("offerId", 0))
    }

    private fun subscribeUi() {
        viewModel.offerDetailsResult.observe(this, Observer {
            offerDetails = it
            showData()
        })
    }

    private fun showData() {
        binding.renter.text = offerDetails.renter.nickname
        binding.scooterComment.text = offerDetails.scooter.comment
        binding.pricePerKm.text = offerDetails.rentOffer.priceKm.toString()
        binding.penalty.text = offerDetails.rentOffer.returnPenalty.toString()
        val dateFormat = SimpleDateFormat("dd-MM-yyyy HH:mm")
        val parsedDate: String = dateFormat.format(offerDetails.rentOffer.returnTime)
        binding.returnTime.text = parsedDate
        if (offerDetails.scooter.photos.isNotEmpty()) initRecycler()
        else binding.recyclerView.visibility = View.GONE
        if (user.isClient) {
            binding.sendInquiry.visibility = View.VISIBLE
        } else {
            binding.sendInquiry.visibility = View.GONE
        }
        when {
            offerDetails.client.isEmpty() -> {
                binding.status.text = "Offer active"
                binding.sendInquiry.visibility = View.VISIBLE
            }
            offerDetails.rentOffer.returnTime.time > System.currentTimeMillis() -> {
                binding.status.text = "Offer active"
                binding.sendInquiry.visibility = View.VISIBLE
            }
            offerDetails.client[0].id == user.id -> {
                binding.status.text = "Your ride"
                binding.sendInquiry.visibility = View.GONE
            }
            else -> {
                binding.status.text = "Offer not available"
                binding.sendInquiry.visibility = View.GONE
            }
        }
    }

    private fun initRecycler() {
        offerDetails.scooter.photos.forEach {
            scooterPhotos.add(BitmapBase64StringConverter.getBitmapFromBase64String(it.imgURL))
        }
        scooterPhotosAdapter = ScooterPhotosAdapter(scooterPhotos) { bitmap, position ->
            false
        }
        binding.recyclerView.adapter = scooterPhotosAdapter
        scooterPhotosAdapter.notifyDataSetChanged()
    }

    fun sendInquiry() {
        if (user.isClient && binding.clientText.text.toString().isNotBlank())
            viewModel.sendRequestForOffer(
                user.id,
                intent.getIntExtra("offerId", 0),
                binding.clientText.text.toString()
            )
    }

}