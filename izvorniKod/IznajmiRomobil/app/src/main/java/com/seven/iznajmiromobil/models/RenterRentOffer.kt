package com.seven.iznajmiromobil.models

import java.sql.Timestamp

class RenterRentOffer {

    var id: Int = 0

    // client data
    // 1. check if clientID == null -> then do NOT read other "client" fields
    // 2. else read other "client" fields
    var clientID: Int? = null
    var clientEmail: String? = null
    var clientName: String? = null
    var clientSurname: String? = null
    var clientNickname: String = ""


    // scooter data (scooter connected to the offer)
    var scooterId: Int = 0
    var scooterComment: String = ""
    var scooterPhotos: List<ScooterPhoto> = arrayListOf()

    var currentLocationLatitude: Double = 0.0
    var currentLocationLongitude: Double = 0.0
    var returnLocationLatitude: Double = 0.0
    var returnLocationLongitude: Double = 0.0
    var returnTime: Timestamp = Timestamp(System.currentTimeMillis())
    var priceKm: Double = 0.0
    var returnPenalty: Double = 0.0
}