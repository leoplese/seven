package com.seven.iznajmiromobil.view.registeredUser.renter

import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.core.app.NavUtils
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.seven.iznajmiromobil.R
import com.seven.iznajmiromobil.databinding.CreateRatingActivityBinding
import com.seven.iznajmiromobil.view.base.BaseActivity
import com.seven.iznajmiromobil.viewmodel.UserViewModel
import javax.inject.Inject

class CreateRatingActivity : BaseActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: CreateRatingActivityBinding

    private lateinit var userViewModel: UserViewModel

    private var rentOfferId: Int? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.create_rating_activity)
        binding.activity = this
        userViewModel = ViewModelProviders.of(this, viewModelFactory)[UserViewModel::class.java]

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        subscribeUi()

        // TODO this part should be in the activity with rent offer details (on "Start transaction" button click)
        // - put extra rentOfferId to intent on startActivity for CreateTransactionActivity
        rentOfferId = intent.getIntExtra("rentOfferId", -1)

    }

    private fun subscribeUi() {
        userViewModel.createRatingResult.observe(this, Observer { data ->
            Toast.makeText(this, "Rating published successfully.", Toast.LENGTH_SHORT).show()
        })
    }

    fun publishRating() {
        AlertDialog.Builder(this)
            .setCancelable(false)
            .setTitle(R.string.discard_changes)
            .setMessage(R.string.are_you_sure_discard_transaction)
            .setPositiveButton(R.string.yes,
                DialogInterface.OnClickListener { dialogInterface, i ->
                    userViewModel.createRating(rentOfferId!!, binding.ratingBar.rating.toInt(), binding.ratingCommentInputEditText.text.toString())
                })
            .setNegativeButton(R.string.no,
                DialogInterface.OnClickListener { dialogInterface, i ->
                    dialogInterface.dismiss()
                })
            .show()
    }

    fun cancelRating() {
        showPopupDiscardRating("showPopupDiscardRating")
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> showPopupDiscardRating("upBtn")
        }

        return true
    }


    override fun onBackPressed() {
        showPopupDiscardRating("backBtn")
        return
    }

    private fun showPopupDiscardRating(discardType: String) {

        AlertDialog.Builder(this)
            .setCancelable(false)
            .setTitle(R.string.discard_changes)
            .setMessage(R.string.are_you_sure_discard_rating)
            .setPositiveButton(R.string.yes,
                DialogInterface.OnClickListener { dialogInterface, i ->
                    when (discardType) {
                        "discardBtn" -> finish()
                        "upBtn" -> NavUtils.navigateUpFromSameTask(this)
                        "backBtn" -> super.onBackPressed()
                    }
                })
            .setNegativeButton(R.string.no,
                DialogInterface.OnClickListener { dialogInterface, i ->
                    dialogInterface.dismiss()
                })
            .show()
    }
}
