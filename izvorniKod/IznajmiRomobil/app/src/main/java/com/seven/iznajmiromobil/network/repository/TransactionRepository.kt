package com.seven.iznajmiromobil.network.repository

import com.seven.iznajmiromobil.network.api.TransactionApi

class TransactionRepository(private val transactionApi: TransactionApi) {

    fun getUserTransactions(userEmail: String) = transactionApi.getUserTransactions(userEmail)


}