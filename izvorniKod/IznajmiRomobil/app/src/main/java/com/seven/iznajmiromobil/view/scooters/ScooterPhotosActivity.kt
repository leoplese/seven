package com.seven.iznajmiromobil.view.scooters

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.seven.iznajmiromobil.R
import com.seven.iznajmiromobil.databinding.ScooterPhotosActivityBinding
import com.seven.iznajmiromobil.models.Scooter
import com.seven.iznajmiromobil.models.ScooterPhoto
import com.seven.iznajmiromobil.view.BitmapBase64StringConverter
import com.seven.iznajmiromobil.view.base.BaseActivity
import com.seven.iznajmiromobil.viewmodel.UserViewModel
import javax.inject.Inject

class ScooterPhotosActivity : BaseActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: UserViewModel
    private lateinit var binding: ScooterPhotosActivityBinding

    private lateinit var scooter: Scooter
    private lateinit var adapter: ScooterPhotosAdapter

    private val photos = mutableListOf<Bitmap>()
    private val addedPhotos = mutableListOf<Bitmap>()

    companion object {
        fun buildIntent(context: Context, scooter: Scooter): Intent {
            val intent = Intent(context, ScooterPhotosActivity::class.java)
            intent.putExtra("scooter", scooter)
            return intent
        }
        private const val IMAGE_PICK_CODE = 1000
        private const val PERMISSION_CODE = 1001
    }

    private var counter = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.scooter_photos_activity)
        binding.activity = this
        viewModel = ViewModelProviders.of(this, viewModelFactory)[UserViewModel::class.java]
        scooter = intent.getSerializableExtra("scooter") as Scooter
        showPhotos()
        subscribeUi()
    }

    private fun showPhotos() {
        scooter.photos.toMutableList()
        scooter.photos.forEach {
            photos.add(BitmapBase64StringConverter.getBitmapFromBase64String(it.imgURL))
        }
        adapter = ScooterPhotosAdapter(photos) { bitmap, position ->
            false
        }
        binding.recyclerView.adapter = adapter
        adapter.notifyDataSetChanged()
    }

    private fun subscribeUi() {
        viewModel.addScooterPhotosResult.observe(this, Observer {
            counter += 1
            if (counter >= addedPhotos.size) finish()
        })
        viewModel.deleteScooterPhotosResult.observe(this, Observer {
            scooter = it
            updatePhotos()
        })

    }

    private fun updatePhotos() {
        photos.clear()
        scooter.photos.forEach {
            photos.add(BitmapBase64StringConverter.getBitmapFromBase64String(it.imgURL))
        }
        adapter.notifyDataSetChanged()
    }

    fun finishEditing() {
        addedPhotos.forEach {
            val imgUrl = BitmapBase64StringConverter.getBase64StringFromBitmap(it)
            viewModel.addScooterPhotos(scooter.id, imgUrl)
        }
    }

    fun add() {
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, IMAGE_PICK_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == IMAGE_PICK_CODE){
            val selectedImageUri = data!!.data
            val imageStream = contentResolver.openInputStream(selectedImageUri!!)
            val selectedImage = BitmapFactory.decodeStream(imageStream)
            photos.add(selectedImage)
            addedPhotos.add(selectedImage)
            adapter.notifyDataSetChanged()
        }
    }
}