package com.seven.iznajmiromobil.view.registeredUser.renter

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.MenuItem
import android.widget.AdapterView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.gson.Gson
import com.seven.iznajmiromobil.R
import com.seven.iznajmiromobil.databinding.ClientReportsActivityBinding
import com.seven.iznajmiromobil.models.ClientReport
import com.seven.iznajmiromobil.models.ClientReportsForReview
import com.seven.iznajmiromobil.models.User
import com.seven.iznajmiromobil.view.base.BaseActivity
import com.seven.iznajmiromobil.view.registeredUser.adapters.ClientReportAdapter
import com.seven.iznajmiromobil.view.registeredUser.mainActivities.commonFunctions.CommonFunctionsClass
import com.seven.iznajmiromobil.viewmodel.UserViewModel
import javax.inject.Inject

class ClientReportsActivity : BaseActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: ClientReportsActivityBinding

    private lateinit var userViewModel: UserViewModel

    private val SHARED_PREF_NAME = "userSharedPrefs"
    private lateinit var sharedPreferences: SharedPreferences

    private lateinit var user: User

    private val USER_SHARED_PREF_KEY: String = "userData"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.client_reports_activity)
        binding.activity = this
        userViewModel = ViewModelProviders.of(this, viewModelFactory)[UserViewModel::class.java]

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        sharedPreferences = getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)

        // get stored user data
        val json = sharedPreferences.getString(USER_SHARED_PREF_KEY, null)
        user = Gson().fromJson(json, User::class.java)

        val clientReportsJson = intent.getStringExtra("clientReports")
        val clientReportsList = Gson().fromJson(clientReportsJson, ClientReportsForReview::class.java).clientReports


        var clientReportsListView = binding.rootLayout
        var clientReportAdapter: ClientReportAdapter = ClientReportAdapter(clientReportsList, this)
        clientReportsListView.adapter = clientReportAdapter

        clientReportsListView.onItemClickListener = AdapterView.OnItemClickListener { parent, view, position, id ->
            val clientReport = parent.getItemAtPosition(position) as ClientReport

            val clientReportDetailsActivityIntent = Intent(this, ClientReportDetailsActivity::class.java)
            clientReportDetailsActivityIntent.putExtra("clientReport", Gson().toJson(clientReport))
            startActivity(clientReportDetailsActivityIntent)
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home ->   CommonFunctionsClass.startActivityBasedOnRoles(this, user.isClient, user.isRenter)
        }

        return true
    }



}
