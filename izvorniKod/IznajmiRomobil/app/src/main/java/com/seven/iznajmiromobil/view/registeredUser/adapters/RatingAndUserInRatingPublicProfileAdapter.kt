package com.seven.iznajmiromobil.view.registeredUser.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.RatingBar
import android.widget.TextView
import com.seven.iznajmiromobil.R
import com.seven.iznajmiromobil.models.RatingAndUserInRatingPublicProfile

class RatingAndUserInRatingPublicProfileAdapter(items: ArrayList<RatingAndUserInRatingPublicProfile>, context: Context) : ArrayAdapter<RatingAndUserInRatingPublicProfile>(context, R.layout.rating_in_user_profile_list_item, items) {

    private class RatingAndUserInRatingPublicProfileViewHolder {

        internal  var userNicknameTv: TextView? = null
        /*
        // see TODO comment in  R.layout.rating_in_user_profile_list_item
        internal  var userNameTv: TextView? = null
        internal  var userSurnameTv: TextView? = null
        internal  var userEmailTv: TextView? = null
         */
        internal  var ratingBar: RatingBar? = null
        internal  var ratingCommentTv: TextView? = null

    }
    override fun getView(i: Int, view: View?, viewGroup: ViewGroup): View {

        var view = view
        val viewHolder: RatingAndUserInRatingPublicProfileViewHolder

        if (view == null) {
            val inflater = LayoutInflater.from(context)
            view = inflater.inflate(R.layout.rating_in_user_profile_list_item, viewGroup, false)

            viewHolder = RatingAndUserInRatingPublicProfileViewHolder()

            viewHolder.userNicknameTv = view.findViewById(R.id.user_nickname_tv) as TextView
            /*
            // see TODO comment in  R.layout.rating_in_user_profile_list_item
            viewHolder.userNameTv = view.findViewById(R.id.user_name_tv) as TextView
            viewHolder.userSurnameTv = view.findViewById(R.id.user_surname_tv) as TextView
            viewHolder.userEmailTv = view.findViewById(R.id.user_email_tv) as TextView
             */
            viewHolder.ratingBar = view.findViewById(R.id.rating_bar) as RatingBar
            viewHolder.ratingCommentTv = view.findViewById(R.id.rating_comment_tv) as TextView

        } else {
            //no need to call findViewById, can use existing ones from saved view holder
            viewHolder = view.tag as RatingAndUserInRatingPublicProfileViewHolder
        }

        val ratingAndUserInRatingPublicProfile = getItem(i)
        viewHolder.userNicknameTv!!.text = ratingAndUserInRatingPublicProfile.userInRatingNickname
        /*
        // see TODO comment in  R.layout.rating_in_user_profile_list_item
        viewHolder.userNameTv!!.text = ratingAndUserInRatingPublicProfile.userInRatingName
        viewHolder.userSurnameTv!!.text = ratingAndUserInRatingPublicProfile.userInRatingSurname
        viewHolder.userEmailTv!!.text = ratingAndUserInRatingPublicProfile.userInRatingEmail
        */
        viewHolder.ratingBar!!.rating = ratingAndUserInRatingPublicProfile!!.rating.toFloat()
        viewHolder.ratingCommentTv!!.text = ratingAndUserInRatingPublicProfile.comment


        view!!.tag = viewHolder

        return view
    }

}