package com.seven.iznajmiromobil.models

import java.sql.Timestamp

class AdvertisedCurrentRentOffer {

    var id: Int = 0

    var currentLocationLatitude: Double = 0.0
    var currentLocationLongitude: Double = 0.0
    var returnLocationLatitude: Double = 0.0
    var returnLocationLongitude: Double = 0.0
    var returnTime: Timestamp = Timestamp(System.currentTimeMillis())
    var priceKm: Double = 0.0
    var returnPenalty: Double = 0.0

    // scooter data (scooter connected to the offer)
    var scooterComment: String = ""
    var scooterPhotos: List<ScooterPhoto> = arrayListOf()

}