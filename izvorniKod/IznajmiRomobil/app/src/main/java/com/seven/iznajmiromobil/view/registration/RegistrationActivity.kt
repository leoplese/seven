package com.seven.iznajmiromobil.view.registration

import android.content.Context
import android.content.Intent
import android.database.Cursor
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.widget.CheckBox
import android.widget.Toast
import androidx.core.view.children
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.textfield.TextInputLayout
import com.seven.iznajmiromobil.R
import com.seven.iznajmiromobil.databinding.RegistrationActivityBinding
import com.seven.iznajmiromobil.utils.TextUtils
import com.seven.iznajmiromobil.view.BitmapBase64StringConverter
import com.seven.iznajmiromobil.view.base.BaseActivity
import com.seven.iznajmiromobil.viewmodel.UserViewModel
import java.io.File
import javax.inject.Inject


class RegistrationActivity : BaseActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: RegistrationActivityBinding
    private lateinit var userViewModel: UserViewModel

    private val idRequestCode = 123
    private val certificateRequestCode = 321

    private var idFilePath = ""
    private var certificateFilePath = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.registration_activity)
        binding.activity = this
        userViewModel = ViewModelProviders.of(this, viewModelFactory)[UserViewModel::class.java]
        subscribeUi()
    }

    private fun subscribeUi() {
        userViewModel.registerResult.observe(this, Observer { data ->
            Toast.makeText(
                this,
                "Registration successful. You will get password on email after admin verification.",
                Toast.LENGTH_SHORT
            ).show()
        })
    }

    private fun toggleCheckbox(checkbox: CheckBox) {
        checkbox.isChecked = !checkbox.isChecked
    }

    private fun getDocument(requestCode: Int) {
        val intent = Intent()
            .setType("image/jpg")
            .setAction(Intent.ACTION_GET_CONTENT)
        startActivityForResult(Intent.createChooser(intent, "Select a JPG file"), requestCode)
    }

    fun attachDocument(type: String) {
        when (type) {
            "id" -> getDocument(idRequestCode)
            "certificate" -> getDocument(certificateRequestCode)
        }
    }

    fun roleClicked(type: String) {
        when (type) {
            "renter" -> toggleCheckbox(binding.renterCheckbox)
            "client" -> toggleCheckbox(binding.clientCheckbox)
        }
    }

    private fun setErrorMessages(incorrectFields: List<TextUtils.TextInputError>) {
        incorrectFields.forEach { error ->
            val textInputLayout = findViewById<TextInputLayout>(error.layoutId)
            textInputLayout.error = error.errorMessage
        }
        binding.rootLayout.children.forEach loop@{ view ->
            if (view is TextInputLayout) {
                if (incorrectFields.any { it.layoutId == view.id }) return@loop
                view.isErrorEnabled = false
            }
        }
    }

    fun register() {
        val incorrectFields = TextUtils.isRegisterDataValid(
            binding.nameInputEditText.text.toString(),
            binding.surnameInputEditText.text.toString(),
            binding.usernameInputEditText.text.toString(),
            binding.emailInputEditText.text.toString(),
            binding.cardInputEditText.text.toString()
        )
        setErrorMessages(incorrectFields)
        if (incorrectFields.isEmpty())
            userViewModel.register(
                binding.nameInputEditText.text.toString(),
                binding.surnameInputEditText.text.toString(),
                binding.usernameInputEditText.text.toString(),
                binding.emailInputEditText.text.toString(),
                binding.cardInputEditText.text.toString(),
                binding.renterCheckbox.isChecked,
                binding.clientCheckbox.isChecked,
                idFilePath,
                certificateFilePath
            )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            idRequestCode -> {
                var bitmap: Bitmap
                var idFileUri = data?.data
                if (idFileUri != null) {
                    bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, idFileUri)

                    idFilePath = BitmapBase64StringConverter.getBase64StringFromBitmap(bitmap)


                    val path = getPath(this, idFileUri)
                    val file = File(path)
                    val filename: String = file.getName()
                    binding.attachId.text = filename
                }
            }
            certificateRequestCode -> {
                var bitmap: Bitmap
                var certificateFileUri = data?.data
                if (certificateFileUri != null) {
                    bitmap =
                        MediaStore.Images.Media.getBitmap(this.contentResolver, certificateFileUri)

                    certificateFilePath = BitmapBase64StringConverter.getBase64StringFromBitmap(bitmap)

                    val path = getPath(this, certificateFileUri)
                    val file = File(path)
                    val filename: String = file.getName()
                    binding.attachCertificate.text = filename
                }

            }
        }
    }


    fun getPath(context: Context, uri: Uri): String? {
        if ("content".equals(uri.getScheme(), ignoreCase = true)) {
            val projection = arrayOf("_data")
            var cursor: Cursor? = null
            try {
                cursor = context.getContentResolver().query(uri, projection, null, null, null)
                val column_index: Int = cursor.getColumnIndexOrThrow("_data")
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index)
                }
            } catch (e: Exception) { // Eat it
            }
        } else if ("file".equals(uri.getScheme(), ignoreCase = true)) {
            return uri.getPath()
        }
        return null
    }
}