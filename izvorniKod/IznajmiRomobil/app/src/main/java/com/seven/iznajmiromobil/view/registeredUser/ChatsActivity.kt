package com.seven.iznajmiromobil.view.registeredUser

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.MenuItem
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.gson.Gson
import com.seven.iznajmiromobil.R
import com.seven.iznajmiromobil.databinding.ChatsActivityBinding
import com.seven.iznajmiromobil.models.Chat
import com.seven.iznajmiromobil.models.User
import com.seven.iznajmiromobil.view.base.BaseActivity
import com.seven.iznajmiromobil.view.registeredUser.adapters.ChatAdapter
import com.seven.iznajmiromobil.view.registeredUser.mainActivities.commonFunctions.CommonFunctionsClass
import com.seven.iznajmiromobil.viewmodel.UserViewModel
import javax.inject.Inject

class ChatsActivity : BaseActivity()  {


    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: ChatsActivityBinding

    private lateinit var userViewModel: UserViewModel

    private val SHARED_PREF_NAME = "userSharedPrefs"
    private lateinit var sharedPreferences: SharedPreferences

    private val USER_SHARED_PREF_KEY: String = "userData"

    // keys (constants - val) in sharedPreferences (map to Boolean value)
    private val IS_CLIENT_KEY: String = "IS_CLIENT"
    private val IS_RENTER_KEY: String = "IS_RENTER"

    private lateinit var chatAdapter: ChatAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.chats_activity)
        binding.activity = this
        userViewModel = ViewModelProviders.of(this, viewModelFactory)[UserViewModel::class.java]

        sharedPreferences = getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)

        subscribeUi()

        // get stored user data
        val json = sharedPreferences.getString(USER_SHARED_PREF_KEY, null)
        val user = Gson().fromJson(json, User::class.java)

        userViewModel.getChats(user.email)
    }


    private fun subscribeUi() {
        // when user data get loaded, display them
        userViewModel.getChatsResult.observe(this, Observer { chats ->
            setupRecycler(chats)
        })

    }

    private fun setupRecycler(chats: List<Chat>) {
        chatAdapter = ChatAdapter(chats)
        binding.recyclerView.adapter = chatAdapter
        chatAdapter.notifyDataSetChanged()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> doOnUpAndBackButtonPressed()
        }

        return true
    }

    override fun onBackPressed() {
        doOnUpAndBackButtonPressed()
    }

    private fun doOnUpAndBackButtonPressed() {
        val IS_CLIENT = sharedPreferences.getBoolean(IS_CLIENT_KEY, false)
        val IS_RENTER = sharedPreferences.getBoolean(IS_RENTER_KEY, false)
        CommonFunctionsClass.startActivityBasedOnRoles(this, IS_CLIENT, IS_RENTER)

        finish()
    }
}
