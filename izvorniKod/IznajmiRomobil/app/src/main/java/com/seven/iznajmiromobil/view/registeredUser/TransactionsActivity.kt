package com.seven.iznajmiromobil.view.registeredUser

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.MenuItem
import android.widget.AdapterView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.gson.Gson
import com.seven.iznajmiromobil.R
import com.seven.iznajmiromobil.databinding.TransactionsActivityBinding
import com.seven.iznajmiromobil.models.Transaction
import com.seven.iznajmiromobil.models.User
import com.seven.iznajmiromobil.view.base.BaseActivity
import com.seven.iznajmiromobil.view.registeredUser.adapters.TransactionAdapter
import com.seven.iznajmiromobil.view.registeredUser.mainActivities.commonFunctions.CommonFunctionsClass
import com.seven.iznajmiromobil.viewmodel.UserViewModel
import javax.inject.Inject

class TransactionsActivity : BaseActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: TransactionsActivityBinding

    private lateinit var userViewModel: UserViewModel

    private val SHARED_PREF_NAME = "userSharedPrefs"
    private lateinit var sharedPreferences : SharedPreferences

    private val USER_SHARED_PREF_KEY: String = "userData"

    private lateinit var transactionAdapter: TransactionAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.transactions_activity)
        binding.activity = this
        userViewModel = ViewModelProviders.of(this, viewModelFactory)[UserViewModel::class.java]

        sharedPreferences = getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)

        subscribeUi()

        // get stored user data
        val json = sharedPreferences.getString(USER_SHARED_PREF_KEY, null)
        val user  = Gson().fromJson(json, User::class.java)

        userViewModel.getUserTransactions(user.email)
    }

    private fun subscribeUi() {
        userViewModel.getUserTransactionsResult.observe(this, Observer{ transactions ->
            setupRecycler(transactions)
        })
    }

    private fun setupRecycler(transaction: List<Transaction>) {
        transactionAdapter = TransactionAdapter(transaction) {
            openTransaction(it)
        }
        binding.recyclerView.adapter = transactionAdapter
        transactionAdapter.notifyDataSetChanged()
    }

    private fun openTransaction(transaction: Transaction) {
        val intent = Intent(this, TransactionDetailsActivity::class.java)
        intent.putExtra("transaction", Gson().toJson(transaction))
        startActivity(intent)
    }
}
