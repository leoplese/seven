package com.seven.iznajmiromobil.models

class UserRatings {

    var ID: Int = 0

    // map rating with public data of user who participated in rating (his public profile data)
    var ratingsAsClient: ArrayList<RatingAndUserInRatingPublicProfile> = arrayListOf()
    var ratingsAsRenter: ArrayList<RatingAndUserInRatingPublicProfile> = arrayListOf()

    var ratingAndUserInRatingPublicProfile = RatingAndUserInRatingPublicProfile()

}
