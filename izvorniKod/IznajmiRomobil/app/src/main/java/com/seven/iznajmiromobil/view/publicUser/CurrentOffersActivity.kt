package com.seven.iznajmiromobil.view.publicUser

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.seven.iznajmiromobil.R
import com.seven.iznajmiromobil.databinding.CurrentOffersActivityBinding
import com.seven.iznajmiromobil.models.AdvertisedCurrentRentOffer
import com.seven.iznajmiromobil.view.base.BaseActivity
import com.seven.iznajmiromobil.view.offers.CurrentOfferAdapter
import com.seven.iznajmiromobil.view.offers.OfferDetailsActivity
import com.seven.iznajmiromobil.viewmodel.UserViewModel
import java.sql.Timestamp
import java.util.*
import javax.inject.Inject

class CurrentOffersActivity : BaseActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: CurrentOffersActivityBinding

    private lateinit var userViewModel: UserViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.current_offers_activity)
        binding.activity = this
        userViewModel = ViewModelProviders.of(this, viewModelFactory)[UserViewModel::class.java]

        subscribeUi()

        val timestamp = Timestamp(System.currentTimeMillis())
        userViewModel.getCurrentOffers(timestamp.toString())
    }

    private fun subscribeUi() {
        userViewModel.getCurrentOffersResult.observe(this, Observer { offers ->
            setupRecycler(offers)
        })
    }

    private fun setupRecycler(offers: List<AdvertisedCurrentRentOffer>) {
        val adapter = CurrentOfferAdapter(offers.toMutableList()) { offer ->
            offerClicked(offer)
        }
        binding.recyclerView.adapter = adapter
        adapter.notifyDataSetChanged()
    }

    private fun offerClicked(offer: AdvertisedCurrentRentOffer) {
        startActivity(OfferDetailsActivity.buildIntent(this, offer.id))
    }

}
