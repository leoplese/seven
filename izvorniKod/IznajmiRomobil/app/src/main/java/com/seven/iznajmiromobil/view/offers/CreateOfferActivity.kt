package com.seven.iznajmiromobil.view.offers

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.SpannableStringBuilder
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog
import com.google.gson.Gson
import com.seven.iznajmiromobil.R
import com.seven.iznajmiromobil.databinding.CreateOfferActivityBinding
import com.seven.iznajmiromobil.models.Scooter
import com.seven.iznajmiromobil.models.User
import com.seven.iznajmiromobil.view.base.BaseActivity
import com.seven.iznajmiromobil.viewmodel.UserViewModel
import java.sql.Timestamp
import java.text.SimpleDateFormat
import javax.inject.Inject


class CreateOfferActivity : BaseActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: UserViewModel
    private lateinit var binding: CreateOfferActivityBinding

    private var timestamp: Timestamp? = null

    private val SHARED_PREF_NAME = "userSharedPrefs"
    private val USER_SHARED_PREF_KEY: String = "userData"

    private val scooterList = mutableListOf<Scooter>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.create_offer_activity)
        binding.activity = this
        viewModel = ViewModelProviders.of(this, viewModelFactory)[UserViewModel::class.java]
        binding.returnTime.isFocusable = false
        binding.returnTime.isClickable = true
        val sharedPreferences: SharedPreferences =
            getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
        val json = sharedPreferences.getString(USER_SHARED_PREF_KEY, null)
        val user = Gson().fromJson(json, User::class.java)
        subscribeUi()
        viewModel.getRenterScooters(user.email)
    }

    private fun subscribeUi() {
        viewModel.getRenterScootersResult.observe(this, Observer { scooters ->
            val spinnerIds = scooters.map { it.comment }
            scooterList.addAll(scooters)
            val adapter =
                ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, spinnerIds)
            binding.spinner.adapter = adapter
        })
        viewModel.createOfferResult.observe(this, Observer {
            Toast.makeText(this, "Offer created!", Toast.LENGTH_SHORT).show()
            Handler(Looper.getMainLooper()).postDelayed({
                onBackPressed()
            }, 500)
        })
    }

    fun postOffer() {
        if (isOfferInputValid()) {
            showAlertDialog()
        } else {
            Toast.makeText(this, "Please enter every field.", Toast.LENGTH_SHORT).show()
        }
    }

    private fun finishPosting() {
        var scooterId = 0
        scooterList.forEach {
            if (it.comment == binding.spinner.selectedItem.toString()) scooterId = it.id
        }
        viewModel.createOffer(
            scooterId,
            0.0,
            0.0,
            0.0,
            0.0,
            timestamp!!,
            binding.pricePerKm.text.toString().toDouble(),
            binding.returnPenalty.text.toString().toDouble()
        )
    }

    private fun showAlertDialog() {
        val alertDialog = AlertDialog.Builder(this).create()
        alertDialog.setMessage("Are you sure?")
        alertDialog.setButton(
            AlertDialog.BUTTON_POSITIVE,
            "Yes"
        ) { _, _ ->
            finishPosting()
        }
        alertDialog.setButton(
            AlertDialog.BUTTON_NEGATIVE,
            "No"
        ) { _, _ ->
            alertDialog.dismiss()
        }
        alertDialog.setCancelable(true)
        alertDialog.show()
    }

    private fun isOfferInputValid(): Boolean {
        var isValid = true
        if ((binding.spinner.selectedItem as String).isBlank()) isValid = false
        if (binding.returnTime.text.toString().isBlank()) isValid = false
        if (binding.pricePerKm.text.toString().isBlank()) isValid = false
        if (binding.returnPenalty.text.toString().isBlank()) isValid = false
        return isValid
    }

    fun openTimePicker() {
        SingleDateAndTimePickerDialog.Builder(this)
            .bottomSheet()
            .minutesStep(1)
//            .displayListener {
//                object : SingleDateAndTimePickerDialog.DisplayListener() {
//                    override fun onDisplayed(picker: SingleDateAndTimePicker?) {
//                        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//                    }
//                }
//            }
            .title("Simple")
            .listener {
                val dateFormat = SimpleDateFormat("dd/MM hh:mm")
                val dateString = dateFormat.format(it)
                binding.returnTime.text = SpannableStringBuilder(dateString)
                timestamp = Timestamp(it.time)
            }.display()
    }

}