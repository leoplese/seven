package com.seven.iznajmiromobil.network.api

import com.seven.iznajmiromobil.models.Transaction
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path

interface TransactionApi {

    data class TransactionResponseBody(var transaction: Transaction)

    /**
     * UC 8
     * @param user to get his account data - identified by email
     * @return User transactions data
     */
    @GET("transactions/{userEmail}")
    fun getUserTransactions(@Path("userEmail") userEmail: String): Observable<TransactionResponseBody>

}