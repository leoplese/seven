package com.seven.iznajmiromobil.view.base

import com.seven.iznajmiromobil.IznajmiRomobilApplication
import dagger.android.support.DaggerAppCompatActivity

abstract class BaseActivity : DaggerAppCompatActivity() {

    private fun getApp() = application as IznajmiRomobilApplication

}