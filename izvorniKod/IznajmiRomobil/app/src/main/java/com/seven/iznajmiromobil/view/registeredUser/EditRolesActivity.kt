package com.seven.iznajmiromobil.view.registeredUser

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.SharedPreferences
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.core.app.NavUtils
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.gson.Gson
import com.seven.iznajmiromobil.R
import com.seven.iznajmiromobil.databinding.EditRolesActivityBinding
import com.seven.iznajmiromobil.models.User
import com.seven.iznajmiromobil.view.base.BaseActivity
import com.seven.iznajmiromobil.viewmodel.UserViewModel
import javax.inject.Inject

class EditRolesActivity : BaseActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: EditRolesActivityBinding

    private lateinit var userViewModel: UserViewModel

    private val SHARED_PREF_NAME = "userSharedPrefs"
    private lateinit var sharedPreferences: SharedPreferences

    private val USER_SHARED_PREF_KEY: String = "userData"

    // keys (constants - val) in sharedPreferences (map to Boolean value)
    private val IS_CLIENT_KEY: String = "IS_CLIENT"
    private val IS_RENTER_KEY: String = "IS_RENTER"

    private lateinit var user: User


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.edit_roles_activity)
        binding.activity = this
        userViewModel = ViewModelProviders.of(this, viewModelFactory)[UserViewModel::class.java]

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        sharedPreferences = getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)

        subscribeUi()

        // get stored user data
        val json = sharedPreferences.getString(USER_SHARED_PREF_KEY, null)
        user = Gson().fromJson(json, User::class.java)

        setInitialUserDataUI()

        binding.toolbar.setNavigationOnClickListener {
            doOnUpAndBackButtonPressed("backBtn")
        }
    }

    private fun setInitialUserDataUI() {
        binding.isClientCheckbox.setChecked(user.isClient)
        binding.isRenterCheckbox.setChecked(user.isRenter)
    }

    private fun subscribeUi() {
        userViewModel.changeUserRolesResult.observe(this, Observer { updatedUser ->
            Toast.makeText(this, "Roles updated successfully.", Toast.LENGTH_SHORT).show()

            user.isClient = updatedUser.isClient
            user.isRenter = updatedUser.isRenter
            // update shared prefs with updated user
            writeUpdatedUserSharedPrefs(user)
        })
    }

    private fun writeUpdatedUserSharedPrefs(updatedUser: User) {
        val editor = sharedPreferences.edit()
        editor.putString(USER_SHARED_PREF_KEY, Gson().toJson(updatedUser))
        editor.putBoolean(IS_CLIENT_KEY, updatedUser.isClient)
        editor.putBoolean(IS_RENTER_KEY, updatedUser.isRenter)
        editor.apply()
    }


    fun saveChanges() {
        val isRenterChecked = binding.isRenterCheckbox.isChecked
        val isClientChecked = binding.isClientCheckbox.isChecked


        // first check if anything is different from initial user data - just if something is different, call API
        if (isAnyRoleDifferent(isRenterChecked, isClientChecked)) {
            userViewModel.changeUserRoles(user.email, isRenterChecked, isClientChecked)
        }
    }

    fun discardChanges() {
        showPopupDiscardChanges("discardBtn")
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> doOnUpAndBackButtonPressed("upBtn")
        }

        return true
    }


    override fun onBackPressed() {
        doOnUpAndBackButtonPressed("backBtn")
        return
    }


    private fun doOnUpAndBackButtonPressed(btnType: String) {
        val isRenterChecked = binding.isRenterCheckbox.isChecked
        val isClientChecked = binding.isClientCheckbox.isChecked

        if (isAnyRoleDifferent(isRenterChecked, isClientChecked)) {
            showPopupDiscardChanges(btnType)
        }
        else
            super.onBackPressed()
    }

    private fun showPopupDiscardChanges(discardType: String) {

        AlertDialog.Builder(this)
            .setCancelable(false)
            .setTitle(R.string.discard_changes)
            .setMessage(R.string.are_you_sure_discard_account_roles_changes)
            .setPositiveButton(R.string.yes,
                DialogInterface.OnClickListener { dialogInterface, i ->
                    when (discardType) {
                        "discardBtn" -> setInitialUserDataUI()
                        "upBtn" -> NavUtils.navigateUpFromSameTask(this)
                        "backBtn" -> super.onBackPressed()
                    }

                })
            .setNegativeButton(R.string.no,
                DialogInterface.OnClickListener { dialogInterface, i ->
                dialogInterface.dismiss()
            })
            .show()
    }

    private fun isAnyRoleDifferent(isRenterChecked: Boolean, isClientChecked: Boolean): Boolean {
        return !(user.isRenter == isRenterChecked && user.isClient == isClientChecked)
    }
}
