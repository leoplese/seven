package com.seven.iznajmiromobil.view.offers

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.seven.iznajmiromobil.R
import com.seven.iznajmiromobil.models.AdvertisedCurrentRentOffer
import kotlinx.android.synthetic.main.offer_view_holder.view.*
import java.text.SimpleDateFormat
import java.util.*

class CurrentOfferAdapter(
    private var offers: MutableList<AdvertisedCurrentRentOffer>,
    private var clickListener: (AdvertisedCurrentRentOffer) -> Unit
) :
    RecyclerView.Adapter<CurrentOfferAdapter.OfferViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OfferViewHolder {
        return OfferViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.offer_view_holder,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return offers.size
    }

    override fun onBindViewHolder(holder: OfferViewHolder, position: Int) {
        holder.bind(offers[position], position, clickListener)
    }

    class OfferViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SimpleDateFormat", "SetTextI18n")
        fun bind(
            offer: AdvertisedCurrentRentOffer,
            position: Int,
            clickListener: (AdvertisedCurrentRentOffer) -> Unit
        ) {
            val sdf = SimpleDateFormat("dd-MM HH:mm")
            val netDate = Date(offer.returnTime.time)
            itemView.valid_until.text = "Valid until: ${sdf.format(netDate)}"
            itemView.offer_title.text = "Rent offer ${position + 1}"
            itemView.root_view.setOnClickListener { clickListener(offer) }
        }
    }
}