package com.seven.iznajmiromobil.view.registeredUser.adapters


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.seven.iznajmiromobil.R
import com.seven.iznajmiromobil.models.ClientReport


class ClientReportAdapter (items: ArrayList<ClientReport>, context: Context) : ArrayAdapter<ClientReport>(context, R.layout.client_report_list_item, items) {

    private class ClientReportViewHolder {

        internal  var userNicknameTv: TextView? = null
        /*
        // see TODO comment in  R.layout.rating_in_user_profile_list_item
        internal  var userNameTv: TextView? = null
        internal  var userSurnameTv: TextView? = null
        internal  var userEmailTv: TextView? = null
         */
        internal  var reportTextTv: TextView? = null
        internal  var reportTimeTv: TextView? = null

    }
    override fun getView(position: Int, view: View?, viewGroup: ViewGroup): View {

        var view = view
        val viewHolder: ClientReportViewHolder

        if (view == null) {
            val inflater = LayoutInflater.from(context)
            view = inflater.inflate(R.layout.client_report_list_item, viewGroup, false)

            viewHolder = ClientReportViewHolder()

            viewHolder.userNicknameTv = view.findViewById(R.id.user_nickname_tv) as TextView
            /*
            // see TODO comment in  R.layout.rating_in_user_profile_list_item
            viewHolder.userNameTv = view.findViewById(R.id.user_name_tv) as TextView
            viewHolder.userSurnameTv = view.findViewById(R.id.user_surname_tv) as TextView
            viewHolder.userEmailTv = view.findViewById(R.id.user_email_tv) as TextView
             */
            viewHolder.reportTextTv = view.findViewById(R.id.report_text_tv) as TextView
            viewHolder.reportTimeTv = view.findViewById(R.id.report_time_tv) as TextView

        } else {
            //no need to call findViewById, can use existing ones from saved view holder
            viewHolder = view.tag as ClientReportViewHolder
        }

        val clientReport = getItem(position)
        viewHolder.userNicknameTv!!.text = clientReport.clientNickname
        /*
        // see TODO comment in  R.layout.rating_in_user_profile_list_item
        viewHolder.userNameTv!!.text = ratingAndUserInRatingPublicProfile.userInRatingName
        viewHolder.userSurnameTv!!.text = ratingAndUserInRatingPublicProfile.userInRatingSurname
        viewHolder.userEmailTv!!.text = ratingAndUserInRatingPublicProfile.userInRatingEmail
        */
        viewHolder.reportTextTv!!.text = clientReport.cause
        viewHolder.reportTimeTv!!.text = clientReport.time.toString()



        view!!.tag = viewHolder

        return view
    }

}