package com.seven.iznajmiromobil.view.registeredUser.client

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.os.Handler
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.core.app.NavUtils
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.gson.Gson
import com.seven.iznajmiromobil.R
import com.seven.iznajmiromobil.databinding.SendReportActivityBinding
import com.seven.iznajmiromobil.models.ScooterPhoto
import com.seven.iznajmiromobil.models.ScooterPhotosList
import com.seven.iznajmiromobil.models.User
import com.seven.iznajmiromobil.view.base.BaseActivity
import com.seven.iznajmiromobil.view.registeredUser.adapters.ImageAdapter
import com.seven.iznajmiromobil.viewmodel.UserViewModel
import java.io.IOException
import java.io.InputStream
import java.net.URL
import java.util.*
import javax.inject.Inject

class SendReportActivity : BaseActivity()  {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: SendReportActivityBinding

    private lateinit var userViewModel: UserViewModel

    private val SHARED_PREF_NAME = "userSharedPrefs"
    private lateinit var sharedPreferences: SharedPreferences

    private val USER_SHARED_PREF_KEY: String = "userData"

    private lateinit var user: User

    private var rentOfferId: Int = -1
    private lateinit var scooterPhotos: List<ScooterPhoto>


    private lateinit var imageAdapter: ImageAdapter
    private lateinit var scooterPhotosGridView: GridView
    private lateinit var loadImagesProgressBar: ProgressBar
    private lateinit var loadScooterPhotosLayout: LinearLayout
    private var couldNotLoad = false
    private var loaded = false


    private val ADD_PHOTO = 100
    private var photoFilePath = ""
    private var addPhotosPathsList: ArrayList<String> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.send_report_activity)
        binding.activity = this
        userViewModel = ViewModelProviders.of(this, viewModelFactory)[UserViewModel::class.java]

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        sharedPreferences = getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)

        subscribeUi()

        // get stored user data
        val json = sharedPreferences.getString(USER_SHARED_PREF_KEY, null)
        user  = Gson().fromJson(json, User::class.java)

        rentOfferId = intent.getIntExtra("rentOfferId", -1)
        val scooterPhotosJson = intent.getStringExtra("scooterPhotos")
        scooterPhotos = Gson().fromJson(scooterPhotosJson, ScooterPhotosList::class.java).scooterPhotos


        setSendReportUI()
    }


    private fun subscribeUi() {
        userViewModel.sendReportForOfferResult.observe(this, Observer{ message ->
            // following toast is just ot inform client report has been sent
            Toast.makeText(this, "Report sent!", Toast.LENGTH_SHORT).show()
        })

    }



    private fun setSendReportUI() {
        loadScooterPhotosLayout = binding.loadScooterPhotosToRemoveLayout
        loadImagesProgressBar = binding.removeImagesLoadProgressBar

        scooterPhotosGridView = binding.scooterPhotosToRemoveGrid

        val removeImgBitmapList: ArrayList<Bitmap?> = arrayListOf()
        imageAdapter = ImageAdapter(removeImgBitmapList, this)
        scooterPhotosGridView.adapter = imageAdapter




        val scooterPhotosObjectsList: List<ScooterPhoto> = scooterPhotos

        val scooterPhotosList: ArrayList<String> = arrayListOf()

        scooterPhotosObjectsList.forEach { scooterPhoto ->
            scooterPhotosList.add(scooterPhoto.imgURL)
        }

        val removeScooterPhotosArray = arrayOfNulls<String>(scooterPhotosList.size)
        for (i in 0 until scooterPhotosList.size) {
            removeScooterPhotosArray[i] = scooterPhotosList.get(i)
        }

        val retrieveImageFromUrlAsyncTask = RetrieveImageFromUrlAsyncTask(loadImagesProgressBar, imageAdapter, loaded)
        retrieveImageFromUrlAsyncTask.execute(*removeScooterPhotosArray)

        Handler().postDelayed(Runnable {
            if (!loaded) {
                retrieveImageFromUrlAsyncTask.cancel(true)
                couldNotLoadImages()
            }
        }, 15000L)

    }

    inner class RetrieveImageFromUrlAsyncTask(var loadImagesProgressBar: ProgressBar, var imageAdapter: ImageAdapter, var loaded: Boolean) : AsyncTask<String, Int, ArrayList<Bitmap>>() {

        override fun doInBackground(vararg imgUrls: String): ArrayList<Bitmap> {

            var bitmapList: ArrayList<Bitmap> = arrayListOf()
            val count = imgUrls.size

            for (i in 0 until count) {
                var bitmap: Bitmap? = null
                try {
                    val inStream: InputStream = URL(imgUrls[i]).openStream()
                    bitmap = BitmapFactory.decodeStream(inStream)
                    if (bitmap != null)
                        bitmapList.add(bitmap)
                    publishProgress(((i + 1) / count.toFloat() * 100).toInt())
                } catch (ex: IOException) { return arrayListOf() }

            }
            return bitmapList
        }

        override fun onProgressUpdate(vararg values: Int?) {
            loadImagesProgressBar.progress = values[0]!!
            //Toast.makeText(this@ClientReportDetailsActivity,"Downloaded ${values[0]} %",Toast.LENGTH_SHORT).show()
            super.onProgressUpdate(values[0])
        }
        override fun onPostExecute(result: ArrayList<Bitmap>) {
            super.onPostExecute(result)
            if (result.isEmpty()) {
                couldNotLoadImages()

            } else {
                imageAdapter.addAll(result)
                loaded = true
            }
        }

    }


    private fun couldNotLoadImages() {
        if (couldNotLoad)
            return

        var couldNotLoadPhotosTv = findViewById<TextView>(R.id.remove_could_not_load_scooter_photos_tv)
        couldNotLoadPhotosTv.visibility = View.VISIBLE
        loadScooterPhotosLayout.visibility = View.GONE
        couldNotLoadPhotosTv.text = "Cannot load scooter images"
        Toast.makeText(this@SendReportActivity, "Cannot load scooter images", Toast.LENGTH_SHORT).show()

        couldNotLoad = true
    }



    fun addScooterPhotos() {


            val intent = Intent()
                .setType("image/*")
                .setAction(Intent.ACTION_GET_CONTENT)
                .putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
            startActivityForResult(Intent.createChooser(intent, "Select a JPG file"), ADD_PHOTO)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            ADD_PHOTO -> {
                photoFilePath = data?.data.toString()
                val decodedFilePath = Uri.decode(photoFilePath)
                addPhotosPathsList.add(decodedFilePath)
            }
        }
    }


    fun sendReport() {
        showPopupConfirmSendReport()
    }

    private fun showPopupConfirmSendReport() {
        AlertDialog.Builder(this)
            .setCancelable(false)
            .setTitle(R.string.send_report)
            .setMessage(R.string.are_you_sure_send_report)
            .setPositiveButton(
                R.string.yes,
                DialogInterface.OnClickListener { dialogInterface, i ->
                    //userViewModel.sendReportForOffer(rentOfferId, binding.reportCommentInputEditText.text.toString(),,)
                })
            .setNegativeButton(
                R.string.no,
                DialogInterface.OnClickListener { dialogInterface, i ->
                    dialogInterface.dismiss()
                })
            .show()
    }

    fun cancelReport() {
        showPopupDiscardChanges("cancelBtn")
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> showPopupDiscardChanges("upBtn")
        }

        return true
    }


    override fun onBackPressed() {
        showPopupDiscardChanges("backBtn")
        return
    }

    private fun showPopupDiscardChanges(discardType: String) {

        AlertDialog.Builder(this)
            .setCancelable(false)
            .setTitle(R.string.cancel_report)
            .setMessage(R.string.are_you_sure_cancel_report)
            .setPositiveButton(
                R.string.yes,
                DialogInterface.OnClickListener { dialogInterface, i ->
                    when (discardType) {
                        "cancelBtn" -> NavUtils.navigateUpFromSameTask(this)
                        "upBtn" -> NavUtils.navigateUpFromSameTask(this)
                        "backBtn" -> super.onBackPressed()
                    }

                })
            .setNegativeButton(
                R.string.no,
                DialogInterface.OnClickListener { dialogInterface, i ->
                    dialogInterface.dismiss()
                })
            .show()
    }
}
