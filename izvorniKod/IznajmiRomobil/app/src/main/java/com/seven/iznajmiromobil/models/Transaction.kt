package com.seven.iznajmiromobil.models

import java.sql.Timestamp

class Transaction {

    var ID: Int = 0

    var distanceCovered: Double = 0.0
    var price: Double = 0.0
    var pentalty: Boolean = false
    var time: Timestamp = Timestamp(System.currentTimeMillis())

    // rent offer data (offer to which the transaction is connected)

    var currentLocationLatitude: Double = 0.0
    var currentLocationLongitude: Double = 0.0
    var returnLocationLatitude: Double = 0.0
    var returnLocationLongitude: Double = 0.0
    var returnTime: Timestamp = Timestamp(System.currentTimeMillis())
    var priceKm: Double = 0.0
    var returnPenalty: Double = 0.0

    // scooter data (scooter connected to the offer)
    var scooterComment: String = ""
    var scooterPhotos: List<ScooterPhoto> = arrayListOf()

    // rating data (rating to which the transaction is connected)
    // rating data CAN be null (if there is no rating related to the transaction)
    var rating: Int? = 0
    var ratingComment: String? = ""

    // data about user who participated in transaction
    // nickname must not be null, other data are non-null if they have public visibility
    var userInTransactionEmail: String? = null
    var userInTransactionName: String? = null
    var userInTransactionSurname: String? = null
    var userInTransactionNickname: String = ""


}