package com.seven.iznajmiromobil.view.registeredUser

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.gson.Gson
import com.seven.iznajmiromobil.R
import com.seven.iznajmiromobil.databinding.SettingsActivityBinding
import com.seven.iznajmiromobil.models.User
import com.seven.iznajmiromobil.view.base.BaseActivity
import com.seven.iznajmiromobil.view.login.LoginActivity
import com.seven.iznajmiromobil.view.registeredUser.mainActivities.commonFunctions.CommonFunctionsClass
import com.seven.iznajmiromobil.viewmodel.UserViewModel
import javax.inject.Inject

class SettingsActivity : BaseActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: SettingsActivityBinding

    private lateinit var userViewModel: UserViewModel

    private val SHARED_PREF_NAME = "userSharedPrefs"
    private lateinit var sharedPreferences: SharedPreferences

    private val USER_SHARED_PREF_KEY: String = "userData"

    // keys (constants - val) in sharedPreferences (map to Boolean value)
    private val IS_CLIENT_KEY: String = "IS_CLIENT"
    private val IS_RENTER_KEY: String = "IS_RENTER"

    private lateinit var user: User


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.settings_activity)
        binding.activity = this
        userViewModel = ViewModelProviders.of(this, viewModelFactory)[UserViewModel::class.java]

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        sharedPreferences = getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)

        subscribeUi()

        // get stored user data
        val json = sharedPreferences.getString(USER_SHARED_PREF_KEY, null)
        user = Gson().fromJson(json, User::class.java)

        // on creating activity instantly call API to get user data and display it
        userViewModel.getAccountData(user.email)

        binding.toolbar.setNavigationOnClickListener{
            doOnUpAndBackButtonPressed()
        }
    }

    private fun subscribeUi() {

        // when user data get loaded, display them
        userViewModel.getAccountDataResult.observe(this, Observer { accountDataResponseBody ->
            val userData = accountDataResponseBody.user
            val showUserData = accountDataResponseBody.showUserData
            binding.myName.setText(userData.name)
            binding.mySurname.setText(userData.surname)
            binding.myNickname.setText(userData.nickname)
            binding.myCreditCardNumber.setText(userData.creditCardNumber)
            binding.myEmail.setText(userData.email)

            binding.isClientCheckbox.setChecked(userData.isClient)
            binding.isRenterCheckbox.setChecked(userData.isRenter)

            binding.isNamePublic.setChecked(showUserData.showName)
            binding.isSurnamePublic.setChecked(showUserData.showSurname)
            binding.isEmailPublic.setChecked(showUserData.showEmail)

            val tempPassword = user.password
            user.name = userData.name
            user.surname = userData.surname
            user.nickname = userData.nickname
            user.creditCardNumber = userData.creditCardNumber
            user.email = userData.email
            user.showName = showUserData.showName
            user.showSurname = showUserData.showSurname
            user.showEmail = showUserData.showEmail
            val editor = sharedPreferences.edit()
            editor.putString(USER_SHARED_PREF_KEY, Gson().toJson(user))
            editor.commit()


        })


        // "DELETE ACCOUNT" btn clicked
        userViewModel.deleteUserResult.observe(this, Observer { data ->
            Toast.makeText(this, "Deleting account successful.", Toast.LENGTH_SHORT).show()

            //clear shared prefs
            val editor = sharedPreferences.edit()
            editor.clear()
            editor.commit()

            //restart app
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        })

        // "EDIT PERSONAL INFO" btn clicked - do nothing HERE but observe change account data / data visibility result in EditPersonalDataActivity's subscribeUI method

        // "EDIT ROLES" btn clicked - do nothing HERE but observe change account data / data visibility result in EditRolesActivity's subscribeUI method

    }


    fun editUserPersonalInfo() {
        startActivityForResult(Intent(this, EditPersonalDataActivity::class.java), 1)
    }

    fun editUserRoles() {
        startActivityForResult(Intent(this, EditRolesActivity::class.java), 2)
    }


    fun deleteUserAccount() {

        AlertDialog.Builder(this)
            .setCancelable(false)
            .setTitle(R.string.delete_account)
            .setMessage(R.string.delete_account_msg)
            .setPositiveButton(R.string.yes,
                DialogInterface.OnClickListener { dialogInterface, i ->
                    userViewModel.deleteUser(binding.myEmail.text.toString())

                })
            .setNegativeButton(R.string.no,
                DialogInterface.OnClickListener { dialogInterface, i ->
                })
            .show()
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> doOnUpAndBackButtonPressed()
        }

        return true
    }

    override fun onBackPressed() {
        doOnUpAndBackButtonPressed()
    }

    private fun doOnUpAndBackButtonPressed() {
        val IS_CLIENT = sharedPreferences.getBoolean(IS_CLIENT_KEY, false)
        val IS_RENTER = sharedPreferences.getBoolean(IS_RENTER_KEY, false)
        CommonFunctionsClass.startActivityBasedOnRoles(this, IS_CLIENT, IS_RENTER)

        finish()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val tempUser = user
        val json = sharedPreferences.getString(USER_SHARED_PREF_KEY, null)
        val user = Gson().fromJson(json, User::class.java)

        when (requestCode) {
            1 -> {
                binding.myName.setText(user.name)
                binding.mySurname.setText(user.surname)
                binding.myNickname.setText(user.nickname)
                binding.myCreditCardNumber.setText(user.creditCardNumber)
                binding.myEmail.setText(user.email)

                binding.isClientCheckbox.setChecked(tempUser.isClient)
                binding.isRenterCheckbox.setChecked(tempUser.isRenter)

                binding.isNamePublic.setChecked(user.showName)
                binding.isSurnamePublic.setChecked(user.showSurname)
                binding.isEmailPublic.setChecked(user.showEmail)
            }
            2 -> {
                binding.myName.setText(tempUser.name)
                binding.mySurname.setText(tempUser.surname)
                binding.myNickname.setText(tempUser.nickname)
                binding.myCreditCardNumber.setText(tempUser.creditCardNumber)
                binding.myEmail.setText(tempUser.email)

                binding.isClientCheckbox.setChecked(user.isClient)
                binding.isRenterCheckbox.setChecked(user.isRenter)

                binding.isNamePublic.setChecked(tempUser.showName)
                binding.isSurnamePublic.setChecked(tempUser.showSurname)
                binding.isEmailPublic.setChecked(tempUser.showEmail)

            }
            else -> {
                Toast.makeText(this, "sdadas", Toast.LENGTH_SHORT)
            }
        }


    }

}
