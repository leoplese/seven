package com.seven.iznajmiromobil.view.scooters

import android.text.Layout
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.seven.iznajmiromobil.R
import com.seven.iznajmiromobil.models.Scooter
import kotlinx.android.synthetic.main.scooter_view_holder.view.*

class ScootersAdapter(val scooters: MutableList<Scooter>, val clickListener: (Scooter) -> Unit) :
    RecyclerView.Adapter<ScootersAdapter.ScooterViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ScooterViewHolder {
        return ScooterViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.scooter_view_holder,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return scooters.size
    }

    override fun onBindViewHolder(holder: ScooterViewHolder, position: Int) {
        holder.bind(scooters[position], clickListener)
    }

    class ScooterViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(scooter: Scooter, clickListener: (Scooter) -> Unit) {
            itemView.scooter_title.text = scooter.comment
            itemView.root_layout.setOnClickListener { clickListener(scooter) }
        }
    }
}