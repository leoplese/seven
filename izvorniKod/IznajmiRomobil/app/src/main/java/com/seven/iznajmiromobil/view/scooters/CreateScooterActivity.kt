package com.seven.iznajmiromobil.view.scooters

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.provider.MediaStore
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.gson.Gson
import com.seven.iznajmiromobil.R
import com.seven.iznajmiromobil.databinding.CreateScooterActivityBinding
import com.seven.iznajmiromobil.models.ScooterPhoto
import com.seven.iznajmiromobil.models.User
import com.seven.iznajmiromobil.view.BitmapBase64StringConverter
import com.seven.iznajmiromobil.view.base.BaseActivity
import com.seven.iznajmiromobil.viewmodel.UserViewModel
import javax.inject.Inject


class CreateScooterActivity : BaseActivity() {

    companion object {
        //image pick code
        private const val IMAGE_PICK_CODE = 1000
        //Permission code
        private const val PERMISSION_CODE = 1001
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: UserViewModel
    private lateinit var binding: CreateScooterActivityBinding

    private lateinit var scooterPhotosAdapter: ScooterPhotosAdapter
    private val scooterPhotos = mutableListOf<Bitmap>()

    private val SHARED_PREF_NAME = "userSharedPrefs"
    private val USER_SHARED_PREF_KEY: String = "userData"

    private lateinit var user: User

    private var counter = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.create_scooter_activity)
        binding.activity = this
        viewModel = ViewModelProviders.of(this, viewModelFactory)[UserViewModel::class.java]
        val sharedPreferences : SharedPreferences = getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
        val json = sharedPreferences.getString(USER_SHARED_PREF_KEY, null)
        user  = Gson().fromJson(json, User::class.java)
        subscribeUi()
        initRecycler()
    }

    private fun subscribeUi() {
        viewModel.createScooterResult.observe(this, Observer {
            if (scooterPhotos.isNotEmpty()) {
                scooterPhotos.forEach { bitmap ->
                    val imgUrl = BitmapBase64StringConverter.getBase64StringFromBitmap(bitmap)
                    viewModel.addScooterPhotos(it.id, imgUrl)
                }
            }
        })
        viewModel.addScooterPhotosResult.observe(this, Observer {
            counter += 1
            if (counter >= scooterPhotos.size) finish()
        })
    }

    private fun initRecycler() {
        scooterPhotosAdapter = ScooterPhotosAdapter(scooterPhotos) { bitmap, pos ->
            showDeleteDialog(pos)
            true
        }
    }

    private fun showDeleteDialog(pos: Int) {
        val alertDialog = AlertDialog.Builder(this).create()
        alertDialog.setMessage("Are you sure you want to delete this photo?")
        alertDialog.setButton(
            AlertDialog.BUTTON_POSITIVE,
            "Yes"
        ) { _, _ ->
            deletePhoto(pos)
        }
        alertDialog.setButton(
            AlertDialog.BUTTON_NEGATIVE,
            "No"
        ) { _, _ ->
            alertDialog.dismiss()
        }
        alertDialog.setCancelable(false)
        alertDialog.show()
    }

    private fun deletePhoto(pos: Int) {
        scooterPhotos.removeAt(pos)
        scooterPhotosAdapter.notifyItemRemoved(pos)
    }

    fun addPhotos() {
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, IMAGE_PICK_CODE)
    }

    fun save() {
        viewModel.createScooter(user.id, binding.commentInputEditText.text.toString(), true)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == IMAGE_PICK_CODE){
            val selectedImageUri = data!!.data
            val imageStream = contentResolver.openInputStream(selectedImageUri!!)
            val selectedImage = BitmapFactory.decodeStream(imageStream)
            scooterPhotos.add(selectedImage)
            scooterPhotosAdapter.notifyDataSetChanged()
        }
    }
}
