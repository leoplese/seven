package com.seven.iznajmiromobil.view.registeredUser

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.Toast
import androidx.core.app.NavUtils
import androidx.core.view.children
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.textfield.TextInputLayout
import com.google.gson.Gson
import com.seven.iznajmiromobil.R
import com.seven.iznajmiromobil.databinding.EditPersonalDataActivityBinding
import com.seven.iznajmiromobil.models.User
import com.seven.iznajmiromobil.utils.TextUtils
import com.seven.iznajmiromobil.view.base.BaseActivity
import com.seven.iznajmiromobil.view.login.LoginActivity
import com.seven.iznajmiromobil.viewmodel.UserViewModel
import kotlinx.android.synthetic.main.change_password_layout.view.*
import javax.inject.Inject

class EditPersonalDataActivity : BaseActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: EditPersonalDataActivityBinding

    private lateinit var userViewModel: UserViewModel

    private val SHARED_PREF_NAME = "userSharedPrefs"
    private lateinit var sharedPreferences: SharedPreferences

    private val USER_SHARED_PREF_KEY: String = "userData"

    private lateinit var user: User

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.edit_personal_data_activity)
        binding.activity = this
        userViewModel = ViewModelProviders.of(this, viewModelFactory)[UserViewModel::class.java]

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        sharedPreferences = getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)

        subscribeUi()

        // get stored user data
        val json = sharedPreferences.getString(USER_SHARED_PREF_KEY, null)
        user = Gson().fromJson(json, User::class.java)

        setInitialUserDataUI()

        binding.toolbar.setNavigationOnClickListener {
            doOnUpAndBackButtonPressed("backBtn")
        }
    }

    private fun setInitialUserDataUI() {
        binding.nameInputEditText.setText(user.name)
        binding.surnameInputEditText.setText(user.surname)
        binding.nicknameInputEditText.setText(user.nickname)
        binding.creditCardNumberInputEditText.setText(user.creditCardNumber)
        binding.emailInputEditText.setText(user.email)

        binding.isNamePublic.setChecked(user.showName)
        binding.isSurnamePublic.setChecked(user.showSurname)
        binding.isEmailPublic.setChecked(user.showEmail)
    }

    private fun subscribeUi() {

        userViewModel.resetPasswordResult.observe(this, Observer { updatedUser ->
            Toast.makeText(this, "Password reset successfully.", Toast.LENGTH_SHORT).show()

            // update shared prefs with updated user
            writeUpdatedUserSharedPrefs(updatedUser, true)
        })

        userViewModel.changeAccountDataResult.observe(this, Observer { updatedUser ->
            Toast.makeText(this, "Account data updated successfully.", Toast.LENGTH_SHORT).show()

            updatedUser.password = user.password

            val isClient = user.isClient
            val isRenter = user.isRenter
            user = updatedUser
            user.isClient = isClient
            user.isRenter = isRenter
            // update shared prefs with updated user
            writeUpdatedUserSharedPrefs(updatedUser, false)

        })
    }

    private fun writeUpdatedUserSharedPrefs(updatedUser: User, clear: Boolean) {
        val editor = sharedPreferences.edit()
        if (!clear)
            editor.putString(USER_SHARED_PREF_KEY, Gson().toJson(updatedUser))
        else {
            editor.clear()
            startActivity(Intent(this, LoginActivity::class.java))
        }
        editor.apply()
    }


    private fun setErrorMessagesChangePassword(
        popupView: View,
        incorrectFields: List<TextUtils.TextInputError>
    ) {
        incorrectFields.forEach { error ->
            val textInputLayout = findViewById<TextInputLayout>(error.layoutId)
            textInputLayout.error = error.errorMessage
        }
        popupView.root_layout.children.forEach loop@{ view ->
            if (view is LinearLayout) {
                view.children.forEach loop@{ childView ->
                    if (childView is TextInputLayout) {
                        if (incorrectFields.any { it.layoutId == childView.id }) return@loop
                        childView.isErrorEnabled = false
                    }
                }
            }
        }
    }

    fun changePassword() {
        val inflater: LayoutInflater =
            getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val popupView = inflater.inflate(R.layout.change_password_layout, null)
        val oldPasswordEditTxt =
            popupView.findViewById<EditText>(R.id.old_password_input_edit_text)
        val newPasswordEditTxt =
            popupView.findViewById<EditText>(R.id.new_password_input_edit_text)
        val retypeNewPasswordEditTxt =
            popupView.findViewById<EditText>(R.id.retype_new_password_input_edit_text)

        val dialog = androidx.appcompat.app.AlertDialog.Builder(this)
            .setView(popupView)
            .setCancelable(false)
            .setTitle(R.string.change_password)
            .setPositiveButton(R.string.save, null)
            .setNegativeButton(R.string.discard, null)
            .create()

        dialog.setOnShowListener {
            val btnPositive = dialog.getButton(androidx.appcompat.app.AlertDialog.BUTTON_POSITIVE)
            btnPositive.setOnClickListener {

                val incorrectFields = TextUtils.isChangePasswordDataValid(
                    oldPasswordEditTxt.text.toString(),
                    newPasswordEditTxt.text.toString(),
                    retypeNewPasswordEditTxt.text.toString()
                )

                setErrorMessagesChangePassword(popupView, incorrectFields)


                if (incorrectFields.isEmpty()) {
                    if (!newPasswordEditTxt.text.toString().equals(retypeNewPasswordEditTxt.text.toString())) {
                        Toast.makeText(
                            this,
                            "New password and retyped new password do not match!",
                            Toast.LENGTH_LONG
                        ).show()
                        newPasswordEditTxt.setText("")
                        retypeNewPasswordEditTxt.setText("")
                    } else {
                        //Toast.makeText(this@EditPersonalDataActivity, "Success!", Toast.LENGTH_LONG).show()
                        userViewModel.resetPassword(
                            user.email,
                            oldPasswordEditTxt.text.toString(),
                            newPasswordEditTxt.text.toString()
                        )
                        dialog.dismiss()
                    }
                }
            }
        }


        dialog.show()
    }

    private fun setErrorMessagesChangeAccountData(incorrectFields: List<TextUtils.TextInputError>) {

        incorrectFields.forEach { error ->
            val textInputLayout = findViewById<TextInputLayout>(error.layoutId)
            textInputLayout.error = error.errorMessage
        }
        binding.rootLayout.children.forEach loop@{ view ->
            if (view is TextInputLayout) {
                if (incorrectFields.any { it.layoutId == view.id }) return@loop
                view.isErrorEnabled = false
            }
        }
    }

    fun saveChanges() {
        val nameEntered = binding.nameInputEditText.text.toString()
        val surnameEntered = binding.surnameInputEditText.text.toString()
        val nicknameEntered = binding.nicknameInputEditText.text.toString()
        val creditCardNumberEntered = binding.creditCardNumberInputEditText.text.toString()
        val emailEntered = binding.emailInputEditText.text.toString()

        val isNamePublic = binding.isNamePublic.isChecked
        val isSurnamePublic = binding.isSurnamePublic.isChecked
        val isEmailPublic = binding.isEmailPublic.isChecked

        val incorrectFields = TextUtils.isChangeAccountPersonalAndVisibilityDataValid(
            nameEntered, surnameEntered, nicknameEntered, creditCardNumberEntered, emailEntered
        )

        setErrorMessagesChangeAccountData(incorrectFields)
        if (incorrectFields.isEmpty()) {
            // first check if anything is different from initial user data - just if something is different, call API
            if (isAnythingDifferent(
                    nameEntered,
                    surnameEntered,
                    nicknameEntered,
                    emailEntered,
                    creditCardNumberEntered,
                    isNamePublic,
                    isSurnamePublic,
                    isEmailPublic
                )
            )
                userViewModel.changeAccountData(
                    nameEntered,
                    surnameEntered,
                    nicknameEntered,
                    emailEntered,
                    creditCardNumberEntered,
                    isNamePublic,
                    isSurnamePublic,
                    isEmailPublic
                )
        }
    }

    fun discardChanges() {
        showPopupDiscardChanges("discardBtn")
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> doOnUpAndBackButtonPressed("upBtn")
        }

        return true
    }


    override fun onBackPressed() {
        doOnUpAndBackButtonPressed("backBtn")
        return
    }


    private fun doOnUpAndBackButtonPressed(btnType: String) {
        val nameEntered = binding.nameInputEditText.text.toString()
        val surnameEntered = binding.surnameInputEditText.text.toString()
        val nicknameEntered = binding.nicknameInputEditText.text.toString()
        val creditCardNumberEntered = binding.creditCardNumberInputEditText.text.toString()
        val emailEntered = binding.emailInputEditText.text.toString()

        val isNamePublic = binding.isNamePublic.isChecked
        val isSurnamePublic = binding.isSurnamePublic.isChecked
        val isEmailPublic = binding.isEmailPublic.isChecked

        if (isAnythingDifferent(
                nameEntered,
                surnameEntered,
                nicknameEntered,
                emailEntered,
                creditCardNumberEntered,
                isNamePublic,
                isSurnamePublic,
                isEmailPublic
            )
        )
            showPopupDiscardChanges(btnType)
        else
            super.onBackPressed()
    }

    private fun showPopupDiscardChanges(discardType: String) {

        AlertDialog.Builder(this)
            .setCancelable(false)
            .setTitle(R.string.discard_changes)
            .setMessage(R.string.are_you_sure_discard_account_roles_changes)
            .setPositiveButton(R.string.yes,
                DialogInterface.OnClickListener { dialogInterface, i ->
                    when (discardType) {
                        "discardBtn" -> setInitialUserDataUI()
                        "upBtn" -> NavUtils.navigateUpFromSameTask(this)
                        "backBtn" -> super.onBackPressed()
                    }

                })
            .setNegativeButton(R.string.no,
                DialogInterface.OnClickListener { dialogInterface, i ->
                    dialogInterface.dismiss()
                })
            .show()
    }


    private fun isAnythingDifferent(
        nameEntered: String,
        surnameEntered: String,
        nicknameEntered: String,
        emailEntered: String,
        creditCardNumberEntered: String,
        isNamePublic: Boolean,
        isSurnamePublic: Boolean,
        isEmailPublic: Boolean
    ): Boolean {
        return !(user.name.equals(nameEntered) && user.surname.equals(surnameEntered) && user.nickname.equals(
            nicknameEntered
        ) && user.email.equals(emailEntered) && user.creditCardNumber.equals(creditCardNumberEntered) &&
                user.showName == isNamePublic && user.showSurname == isSurnamePublic && user.showEmail == isEmailPublic)

    }
}
