package com.seven.iznajmiromobil.di.modules

import com.seven.iznajmiromobil.di.scopes.ActivityScope
import com.seven.iznajmiromobil.view.offers.CurrentOffersFragment
import com.seven.iznajmiromobil.view.offers.PastOffersFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentModule {

    @ContributesAndroidInjector
    @ActivityScope
    abstract fun contributePastOffersFragment(): PastOffersFragment

    @ContributesAndroidInjector
    @ActivityScope
    abstract fun contributeCurrentOffersFragment(): CurrentOffersFragment
}