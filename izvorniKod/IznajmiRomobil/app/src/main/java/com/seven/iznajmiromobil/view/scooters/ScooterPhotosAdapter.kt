package com.seven.iznajmiromobil.view.scooters

import android.graphics.Bitmap
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.seven.iznajmiromobil.R
import com.seven.iznajmiromobil.models.ScooterPhoto
import kotlinx.android.synthetic.main.scooter_photo_view_holder.view.*

class ScooterPhotosAdapter(
    private val photos: MutableList<Bitmap>,
    private val clickListener: (Bitmap, Int) -> Boolean
) : RecyclerView.Adapter<ScooterPhotosAdapter.ScooterPhotoViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ScooterPhotoViewHolder {
        return ScooterPhotoViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.scooter_photo_view_holder,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return photos.size
    }

    override fun onBindViewHolder(holder: ScooterPhotoViewHolder, position: Int) {
        holder.bind(photos[position], clickListener)
    }

    class ScooterPhotoViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(photo: Bitmap, clickListener: (Bitmap, Int) -> Boolean) {
            Glide.with(itemView.image).load(photo).into(itemView.image)
            itemView.root_layout.setOnLongClickListener {
                clickListener(photo, adapterPosition)
            }
        }
    }
}