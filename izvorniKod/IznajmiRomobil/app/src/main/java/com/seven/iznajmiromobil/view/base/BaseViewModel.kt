package com.seven.iznajmiromobil.view.base

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

abstract class BaseViewModel : ViewModel() {

    val error = MutableLiveData<Throwable>()
    val isLoading = MutableLiveData<Boolean>()

    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

    private fun notifyLoadingStarted() {
        isLoading.value = true
    }

    private fun notifyLoadingStopped() {
        isLoading.value = false
    }

    protected fun executeTask(observable: Observable<*>?, success: (Any) -> Unit = {}, failure: (Throwable) -> Unit = {}) {
        observable?.let {
            compositeDisposable.add(it
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { notifyLoadingStarted() }
                .doOnNext { notifyLoadingStopped() }
                .doOnError { throwable ->
                    error.value = throwable
                    notifyLoadingStopped()
                }
                .subscribe(success, failure))
        }
    }

    protected fun executeTaskWithoutLoading(observable: Observable<*>, success: (Any) -> Unit = {}, failure: (Throwable) -> Unit = {}) {
        compositeDisposable.add(observable
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnError { throwable -> error.value = throwable }
            .subscribe(success, failure))
    }
}