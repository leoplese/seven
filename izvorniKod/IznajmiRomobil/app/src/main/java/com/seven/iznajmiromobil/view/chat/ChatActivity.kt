package com.seven.iznajmiromobil.view.chat

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.seven.iznajmiromobil.R
import com.seven.iznajmiromobil.databinding.ChatActivityBinding
import com.seven.iznajmiromobil.models.Message
import com.seven.iznajmiromobil.viewmodel.UserViewModel
import javax.inject.Inject

class ChatActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: ChatActivityBinding
    private lateinit var viewModel: UserViewModel

    private val messages = mutableListOf<Message>()

    private lateinit var adapter: MessagesAdapter

    private var chatId = 0
    private var senderId = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.chat_activity)
        binding.activity = this
        viewModel = ViewModelProviders.of(this, viewModelFactory)[UserViewModel::class.java]
        //get chatId from intent
        adapter = MessagesAdapter(messages)
        subscribeUi()
    }

    private fun subscribeUi() {
        viewModel.getMessagesInChatResult.observe(this, Observer {
            messages.addAll(it)
            setupRecycler()
        })
        viewModel.sendMessageInChatResult.observe(this, Observer { message ->
            messages.add(message)
            adapter.notifyItemInserted(messages.size)
        })
    }

    private fun setupRecycler() {
        binding.recyclerView.adapter = adapter
        messages.sortBy { it.time }
        adapter.notifyDataSetChanged()
    }

    fun sendMessage() {
        if (binding.messageInput.text.toString().isNotBlank()) {
            viewModel.sendMessageInChat(chatId, binding.messageInput.text.toString(), senderId)
        }
    }

}