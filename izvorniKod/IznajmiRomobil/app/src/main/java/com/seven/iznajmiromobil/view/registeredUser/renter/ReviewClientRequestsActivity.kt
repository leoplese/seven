package com.seven.iznajmiromobil.view.registeredUser.renter

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.widget.AdapterView
import android.widget.Toast
import androidx.core.app.NavUtils
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.gson.Gson
import com.seven.iznajmiromobil.R
import com.seven.iznajmiromobil.databinding.ReviewClientRequestsActivityBinding
import com.seven.iznajmiromobil.models.ClientRequest
import com.seven.iznajmiromobil.models.ClientRequestsForReview
import com.seven.iznajmiromobil.view.base.BaseActivity
import com.seven.iznajmiromobil.view.registeredUser.UserPublicProfileActivity
import com.seven.iznajmiromobil.view.registeredUser.adapters.ClientRequestAdapter
import com.seven.iznajmiromobil.viewmodel.UserViewModel
import javax.inject.Inject

class ReviewClientRequestsActivity : BaseActivity()  {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: ReviewClientRequestsActivityBinding

    private lateinit var userViewModel: UserViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.review_client_requests_activity)
        binding.activity = this
        userViewModel = ViewModelProviders.of(this, viewModelFactory)[UserViewModel::class.java]

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        subscribeUi()

        /*
        // TODO this part should be in the activity with rent offer details (on "Review client requests" button click)
            - put extra to intent on startActivity for ReviewClientRequestsActivity
        var rentOffer: RenterRentOffer = RenterRentOffer()
        userViewModel.getClientRequests(rentOffer.ID)
        -in subscribeUI() in Observer: userViewModel.getClientRequestsResult -> get result (data: List<ClientRequest>) and start this activity
        */

        val clientRequestsJson = intent.getStringExtra("clientRequests")
        val clientRequestsList = Gson().fromJson(clientRequestsJson, ClientRequestsForReview::class.java).clientRequests


        var clientRequestsListView = binding.rootLayout
        var clientRequestAdapter: ClientRequestAdapter = ClientRequestAdapter(clientRequestsList, this, userViewModel)
        clientRequestsListView.adapter = clientRequestAdapter

        clientRequestsListView.onItemClickListener = AdapterView.OnItemClickListener { parent, view, position, id ->
            val clientRequest = parent.getItemAtPosition(position) as ClientRequest

            searchUserProfile(clientRequest.clientNickname)
        }


    }

    private fun subscribeUi() {
        // when clicked on a list item, display public profile of the client who sent the clicked request
        userViewModel.getUserPublicProfileResult.observe(this, Observer { userPublicProfile ->
            val userPublicProfileActivityIntent = Intent(this, UserPublicProfileActivity::class.java)
            userPublicProfileActivityIntent.putExtra("userProfile", Gson().toJson(userPublicProfile))
            userPublicProfileActivityIntent.putExtra("sourceActivity", "ReviewClientRequestsActivity")
            startActivity(userPublicProfileActivityIntent)

            finish()
        })

        // API confirmClientRequestForOffer on userViewModel is called in the adapter (if "Approve" button is cliked, confirmed by popup)
        userViewModel.confirmClientRequestForOfferResult.observe(this, Observer{ message ->
            //Toast.makeText(this, "Request confirmed (" + message.time + ")", Toast.LENGTH_LONG).show()
            Toast.makeText(this, message.text, Toast.LENGTH_LONG).show()
        })
    }

    private fun searchUserProfile(clickedUserNickname: String) {
        userViewModel.getUserPublicProfile(clickedUserNickname)
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> NavUtils.navigateUpFromSameTask(this)
        }

        return true
    }

}
