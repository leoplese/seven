package com.seven.iznajmiromobil.di.modules

import com.seven.iznajmiromobil.di.scopes.ActivityScope
import com.seven.iznajmiromobil.view.SplashActivity
import com.seven.iznajmiromobil.view.login.LoginActivity
import com.seven.iznajmiromobil.view.myDrives.MyDrivesActivity
import com.seven.iznajmiromobil.view.offers.CreateOfferActivity
import com.seven.iznajmiromobil.view.offers.OfferDetailsActivity
import com.seven.iznajmiromobil.view.offers.OffersActivity
import com.seven.iznajmiromobil.view.publicUser.CurrentOffersActivity
import com.seven.iznajmiromobil.view.publicUser.RentOfferDetailsActivity
import com.seven.iznajmiromobil.view.registeredUser.*
import com.seven.iznajmiromobil.view.registeredUser.client.SendReportActivity
import com.seven.iznajmiromobil.view.registeredUser.client.SendRequestActivity
import com.seven.iznajmiromobil.view.registeredUser.mainActivities.ClientAndRenterMainActivity
import com.seven.iznajmiromobil.view.registeredUser.mainActivities.ClientMainActivity
import com.seven.iznajmiromobil.view.registeredUser.mainActivities.RenterMainActivity
import com.seven.iznajmiromobil.view.registeredUser.renter.ClientReportDetailsActivity
import com.seven.iznajmiromobil.view.registeredUser.renter.ClientReportsActivity
import com.seven.iznajmiromobil.view.registeredUser.renter.CreateRatingActivity
import com.seven.iznajmiromobil.view.registeredUser.renter.CreateTransactionActivity
import com.seven.iznajmiromobil.view.registration.RegistrationActivity
import com.seven.iznajmiromobil.view.resetPassword.ResetPasswordActivity
import com.seven.iznajmiromobil.view.scooters.CreateScooterActivity
import com.seven.iznajmiromobil.view.scooters.ScooterActivity
import com.seven.iznajmiromobil.view.scooters.ScooterPhotosActivity
import com.seven.iznajmiromobil.view.scooters.ScootersActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {

    @ContributesAndroidInjector
    @ActivityScope
    abstract fun contributeSplashActivity(): SplashActivity

    @ContributesAndroidInjector
    @ActivityScope
    abstract fun contributeLoginActivity(): LoginActivity

    @ContributesAndroidInjector
    @ActivityScope
    abstract fun contributeClientMainActivity(): ClientMainActivity

    @ContributesAndroidInjector
    @ActivityScope
    abstract fun contributeRenterMainActivity(): RenterMainActivity

    @ContributesAndroidInjector
    @ActivityScope
    abstract fun contributeClientAndRenterMainActivity(): ClientAndRenterMainActivity

    @ContributesAndroidInjector
    @ActivityScope
    abstract fun contributeRegistrationActivity(): RegistrationActivity

    @ContributesAndroidInjector
    @ActivityScope
    abstract fun contributeResetPasswordActivity(): ResetPasswordActivity


    @ContributesAndroidInjector
    @ActivityScope
    abstract fun contributeSettingsActivity(): SettingsActivity

    @ContributesAndroidInjector
    @ActivityScope
    abstract fun contributeEditPersonalDataActivity(): EditPersonalDataActivity

    @ContributesAndroidInjector
    @ActivityScope
    abstract fun contributeEditRolesActivity(): EditRolesActivity

    @ContributesAndroidInjector
    @ActivityScope
    abstract fun contributeTransactionsActivity(): TransactionsActivity

    @ContributesAndroidInjector
    @ActivityScope
    abstract fun contributeTransactionDetailsActivity(): TransactionDetailsActivity

    @ContributesAndroidInjector
    @ActivityScope
    abstract fun contributeUserPublicProfileActivity(): UserPublicProfileActivity


    @ContributesAndroidInjector
    @ActivityScope
    abstract fun contributeRatingsActivity(): RatingsActivity

    @ContributesAndroidInjector
    @ActivityScope
    abstract fun contributeCurrentOffersActivity(): CurrentOffersActivity

    @ContributesAndroidInjector
    @ActivityScope
    abstract fun contributeCreateTransactionActivity(): CreateTransactionActivity


    @ContributesAndroidInjector
    @ActivityScope
    abstract fun contributeClientReportsActivity(): ClientReportsActivity

    @ContributesAndroidInjector
    @ActivityScope
    abstract fun contributeClientReportDetailsActivity(): ClientReportDetailsActivity


    @ContributesAndroidInjector
    @ActivityScope
    abstract fun contributeCreateRatingActivity(): CreateRatingActivity

    @ContributesAndroidInjector
    @ActivityScope
    abstract fun contributeChatsActivity(): ChatsActivity


    @ContributesAndroidInjector
    @ActivityScope
    abstract fun contributeRentOfferDetailsActivity(): RentOfferDetailsActivity

    @ContributesAndroidInjector
    @ActivityScope
    abstract fun contributeSendRequestActivity(): SendRequestActivity

    @ContributesAndroidInjector
    @ActivityScope
    abstract fun contributeSendReportActivity(): SendReportActivity

    @ContributesAndroidInjector
    @ActivityScope
    abstract fun contributeScootersActivity(): ScootersActivity

    @ContributesAndroidInjector
    @ActivityScope
    abstract fun contributeScooterActivity(): ScooterActivity

    @ContributesAndroidInjector
    @ActivityScope
    abstract fun contributeCreateScooterActivity(): CreateScooterActivity

    @ContributesAndroidInjector
    @ActivityScope
    abstract fun contributeScooterPhotosActivity(): ScooterPhotosActivity

    @ContributesAndroidInjector
    @ActivityScope
    abstract fun contributeCreateOfferActivity(): CreateOfferActivity

    @ContributesAndroidInjector
    @ActivityScope
    abstract fun contributeOffersActivity(): OffersActivity

    @ContributesAndroidInjector
    @ActivityScope
    abstract fun contributeMyDrivesActivity(): MyDrivesActivity

    @ContributesAndroidInjector
    @ActivityScope
    abstract fun contributeOfferDetailsActivity(): OfferDetailsActivity

}