package com.seven.iznajmiromobil.models

import java.sql.Timestamp

class ClientReport {

    // offer to which this report belongs
    var rentOfferId: Int = 0

    // client who sent request
    var clientEmail: String? = null
    var clientName: String? = null
    var clientSurname: String? = null
    var clientNickname: String = ""

    // cause text in client request message is MANDATORY
    var cause: String = ""
    var photosToAdd: List<ScooterPhoto> = arrayListOf()
    var photosToRemove: List<ScooterPhoto> = arrayListOf()
    var time: Timestamp = Timestamp(System.currentTimeMillis())

}