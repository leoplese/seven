package com.seven.iznajmiromobil.view.base

import dagger.android.support.DaggerFragment

abstract class BaseFragment : DaggerFragment()