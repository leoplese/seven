import decimal

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status, viewsets
from rest_framework.generics import *
from .models import *
from .serializer import *
import datetime
import random
from django.db.models import Q


# treba nam
class UserView(APIView):

    def get(self, request, pk, format=None):
        try:
            tech = user.objects.get(pk=pk)
            serializer = UserSerializer(tech)
            return Response(serializer.data)
        except:
            return Response(status=status.HTTP_404_NOT_FOUND)

    def delete(self, request, pk, format=None):
        tech = user.objects.get(pk=pk)
        tech.delete()
        return Response(status=status.HTTP_200_OK)


# Get rentOfferId
class GetRentOfferId(ListAPIView):
    queryset = rentOffer.objects.all()
    serializer_class = CreateRentOfferSerializer2

    def post(self, request, format=None):
        serializer = CreateRentOfferSerializer2(data=request.data)
        if serializer.is_valid():
            techlist=rentOffer.objects.filter(scooterId=serializer.data['scooterId'])
            ispis=[]
            for tech in techlist:
                serializer2 = GetRentOfferSerializer(tech)
                ispis.append(serializer2.data)
            return Response({'rentOffer': ispis}, status=status.HTTP_200_OK)
        if serializer.is_valid():
            pass
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


'''# za testiranje: vraca ShowUserData
class DataVisibility(UpdateAPIView):
    queryset = user.objects.all()
    serializer_class = EmailSerializer

    def post(self, request, format=None):
        serializer = EmailSerializer(data=request.data)
        if serializer.is_valid():
            var = user.objects.filter(email=serializer.data['email'])
            if var and showUserData.objects.filter(userId=var[0]):
                tech = showUserData.objects.get(userId=var[0])
                serializer = DataVisibilitySerializer(tech)
                return Response({'dataVisibility': serializer.data}, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_404_NOT_FOUND)'''


# za testiranje
class CreateTransaction(ListAPIView):
    queryset = transaction.objects.all()
    serializer_class = TransactionSerializer

    def post(self, request, format=None):
        serializer = TransactionSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({'transaction': serializer.data}, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

#za GetOfferDetails
class GetOfferDetails(ListAPIView):
    queryset = rating.objects.all()
    serializer_class = GetOfferDetailsSerializer

    def post(self, request, format=None):
        serializer = GetOfferDetailsSerializer(data=request.data)
        if serializer.is_valid():
            off=rentOffer.objects.get(id=serializer.data['offer_Id'])
            serializer2=CreateRentOfferSerializer(off)
            scoot=scooter.objects.get(id=off.scooterId.id)
            serializer3=CreateScooterSerializer(scoot)
            rent=user.objects.get(id=scoot.renterId.id)
            serializer4=WholeUserSerializer(rent)
            ispis=[]
            if off.clientId is not None:
                clie=user.objects.get(id=off.clientId.id)
                serializer5=WholeUserSerializer(clie)
                ispis.append(serializer5.data)
            return Response({'rentOffer': serializer2.data,'scooter': serializer3.data,
                             'renter': serializer4.data,'client': ispis}, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# za testiranje
class CreateRating(ListAPIView):
    queryset = rating.objects.all()
    serializer_class = RatingSerializer

    def post(self, request, format=None):
        serializer = RatingSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({'rating': serializer.data}, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# za set passworda
class PasswordSet(UpdateAPIView):
    queryset = user.objects.all()
    serializer_class = LoginSerializer

    def post(self, request, format=None):
        serializer = LoginSerializer(data=request.data)
        if serializer.is_valid():
            if user.objects.get(email=serializer.data['email']):
                u = user.objects.get(email=serializer.data['email'])
                u.password = serializer.data['password']
                u.save()
                serializer2 = WholeUserSerializer(u)
                return Response({'user': serializer2.data}, status=status.HTTP_200_OK)
            else:
                return Response(serializer.errors, status=status.HTTP_404_NOT_FOUND)


# za reset passworda
class PasswordReset(UpdateAPIView):
    queryset = user.objects.all()
    serializer_class = ResetPasswordSerializer

    def post(self, request, format=None):
        serializer = ResetPasswordSerializer(data=request.data)
        if serializer.is_valid():
            if user.objects.get(email=serializer.data['email']):
                u = user.objects.get(email=serializer.data['email'])
                if u.password == serializer.data['oldPassword']:
                    u.password = serializer.data['newPassword']
                    u.save()
                    serializer2 = WholeUserSerializer(u)
                    return Response({'user': serializer2.data}, status=status.HTTP_200_OK)
                else:
                    return Response(serializer.errors, status=status.HTTP_401_UNAUTHORIZED)
            else:
                return Response(serializer.errors, status=status.HTTP_404_NOT_FOUND)


# UC 1 Pregled aktualnih ponuda za najam romobila
class GetActiveOffers(APIView):
    queryset = rentOffer.objects.all()
    serializer_class = DateTimeSerializer

    def post(self, request, format=None):
        serializer = DateTimeSerializer(data=request.data)
        if serializer.is_valid():
            tech = rentOffer.objects.filter(returnTime__gt=serializer.data['dateTime'])
            ispis = []
            for te in tech:
                if te.clientId is None:
                    serializer2 = GetRentOfferSerializer(te)
                    ispis.append(serializer2.data)
            return Response({'rentOffer': ispis}, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# UC2 - Registracija
class Register(ListAPIView):
    queryset = user.objects.all()
    serializer_class = UserSerializer

    def post(self, request, format=None):
        serializer1 = UserSerializer(data=request.data)
        if serializer1.is_valid():
            if (not user.objects.filter(email=serializer1.validated_data['email'])) and (
                    not user.objects.filter(nickname=serializer1.validated_data['nickname'])):
                serializer1.save()
                var = user.objects.filter(email=serializer1.validated_data['email'])
                vars = showUserData.objects.create(userId=var[0], showName=True, showSurname=True, showEmail=True)
                serializer = DataVisibilitySerializer(data=vars)
                if serializer.is_valid():
                    serializer.save()
                return Response({'user': serializer1.data}, status=status.HTTP_201_CREATED)
        return Response(serializer1.errors, status=status.HTTP_400_BAD_REQUEST)


''''# UC2 - Registracija - slike
class RegisterPhotos(ListAPIView):
    queryset = user.objects.all()
    serializer_class = EmailPhotoSerializer

    def post(self, request, format=None):
        serializer = EmailPhotoSerializer(data=request.data)
        if serializer.is_valid():
            currUser=user.objects.get(email=serializer.data['email'])
            currUser.idURL=serializer.data['idURL']
            currUser.recordURL=serializer.data['recordURL']
            currUser.save()
            return Response({'user': serializer.data}, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)'''


# UC3 - Prijava u sustav
class Login(ListAPIView):
    queryset = user.objects.all()
    serializer_class = LoginSerializer

    def post(self, request, format=None):
        serializer = LoginSerializer(data=request.data)
        if serializer.is_valid():
            if (user.objects.get(email=serializer.data['email']).isActive is True) and user.objects.filter(
                    email=serializer.data['email']) and user.objects.get(
                    email=serializer.data['email']).password == serializer.data['password']:
                u = user.objects.get(email=serializer.data['email'])
                serializer2 = WholeUserSerializer(u)
                return Response({'user': serializer2.data}, status=status.HTTP_200_OK)
            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# UC4 - Brisanje korisničkog računa
class DeleteUser(ListAPIView):
    queryset = user.objects.all()
    serializer_class = EmailSerializer

    def post(self, request, format=None):
        serializer = EmailSerializer(data=request.data)
        if serializer.is_valid():
            if user.objects.filter(email=serializer.data['email']):
                user.objects.filter(email=serializer.data['email']).delete()
                return Response(status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_404_NOT_FOUND)


# UC5 - Pregled osobnih podataka
class AccountData(ListAPIView):
    queryset = user.objects.all()
    serializer_class = EmailSerializer

    def post(self, request, format=None):
        serializer = EmailSerializer(data=request.data)
        if serializer.is_valid():
            if user.objects.filter(email=serializer.data['email']):
                tech = user.objects.get(email=serializer.data['email'])
                tech2 = showUserData.objects.get(userId=tech.id)
                serializer = UserSerializer(tech)
                serializer2 = UserSerializer2(tech2)
                return Response({'user': serializer.data, 'showUserData': serializer2.data}, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_404_NOT_FOUND)


# UC6 - Promjena osobnih podataka
class UpdateInfo(ListAPIView):
    queryset = user.objects.all()
    serializer_class = UpdateInfoSerializer

    def post(self, request, format=None):
        serializer = UpdateInfoSerializer(data=request.data)
        if serializer.is_valid():
            try:
                updatedUser = user.objects.get(email=serializer.data['email'])
                updatedUser.name = serializer.data['name']
                updatedUser.surname = serializer.data['surname']
                updatedUser.nickname = serializer.data['nickname']
                updatedUser.creditCardNumber = serializer.data['creditCardNumber']
                # updatedUser.password = serializer.data['password']
                showData = showUserData.objects.get(userId=updatedUser.id)
                showData.showName = serializer.data['showName']
                showData.showSurname = serializer.data['showSurname']
                showData.showEmail = serializer.data['showEmail']
                updatedUser.save()
                showData.save()
                return Response({'user': serializer.data})
            except:
                return Response(serializer.errors, status=status.HTTP_404_NOT_FOUND)


'''# UC7 - Promjena vidljivosti osobnih podataka(radi)
class ChangeDataVisibility(UpdateAPIView):
    queryset = showUserData.objects.all()
    serializer_class = DataVisibilitySerializer

    def post(self, request, format=None):
        serializer = DataVisibilitySerializer(data=request.data)
        if serializer.is_valid():
            showUserData.objects.filter(userId=serializer.validated_data['userId']).delete()
            serializer.save()
            return Response({'dataVisibility': serializer.data}, status=status.HTTP_200_OK)
        if serializer.is_valid():
            pass
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)'''


# UC8 - Pregled vlastitih transakcija hehe
class GetTransactions(ListAPIView):
    queryset = user.objects.all()
    serializer_class = EmailSerializer

    def post(self, request, format=None):
        serializer = EmailSerializer(data=request.data)
        if serializer.is_valid():
            if user.objects.filter(email=serializer.data['email']):
                ispisList = []
                currentUser = user.objects.filter(email=serializer.validated_data['email'])[0]
                scooterList = scooter.objects.filter(renterId=currentUser)
                for sc in scooterList:
                    scooterOffers = rentOffer.objects.filter(scooterId=sc.id)
                    for ro in scooterOffers:
                        try:
                            listRentOffer = ["rentOfferData: "]
                            listTransaction = ["transactionData: "]
                            sclist = ["scooterData: "]
                            scplist = ["scooterPhoto: "]
                            scp2list = []
                            rlist = ["ratingData: "]
                            clientList = ["ClientData: "]
                            renterList = ["RenterData: "]
                            currTrnasaction = transaction.objects.filter(rentOfferId=ro.id)[0]
                            serializer1 = GetTransactionSerializer(currTrnasaction)
                            listTransaction.append(serializer1.data)
                            serializer2 = GetTransactionSerializer2(ro)
                            listRentOffer.append(serializer2.data)
                            serializer3 = GetTransactionSerializer3(sc)
                            sclist.append(serializer3.data)
                            photoList = scooterPhoto.objects.filter(scooterId=sc.id)
                            for ph in photoList:
                                scp2list.append(ph.imgURL)
                            scplist.append(scp2list)
                            ispisList.append(listTransaction)
                            ispisList.append(listRentOffer)
                            sclist.append(scplist)
                            ispisList.append(sclist)
                            currentRating = rating.objects.filter(id=currTrnasaction.ratingId.id)[0]
                            serializer4 = GetTransactionSerializer4(currentRating)
                            rlist.append(serializer4.data)
                            ispisList.append(rlist)

                            curClient = user.objects.filter(id=currentRating.clientId.id)[0]
                            currentshowUserData = showUserData.objects.filter(userId=curClient)[0]
                            if (currentshowUserData.showName == False):
                                currentUser.name = None
                            if (currentshowUserData.showSurname == False):
                                currentUser.surname = None
                            if (currentshowUserData.showEmail == False):
                                currentUser.email = None
                            serializer5 = PublicDataSerializer(curClient)
                            clientList.append(serializer5.data)
                            ispisList.append(clientList)

                            curRenter = user.objects.filter(id=currentRating.renterId.id)[0]
                            currentshowUserData = showUserData.objects.filter(userId=curRenter)[0]
                            if (currentshowUserData.showName == False):
                                currentUser.name = None
                            if (currentshowUserData.showSurname == False):
                                currentUser.surname = None
                            if (currentshowUserData.showEmail == False):
                                currentUser.email = None
                            serializer6 = PublicDataSerializer(curRenter)
                            renterList.append(serializer6.data)
                            ispisList.append(renterList)
                        except:
                            pass
                return Response({'transactions': ispisList}, status=status.HTTP_200_OK)
            else:
                return Response(serializer.errors, status=status.HTTP_404_NOT_FOUND)


# UC9/10 - Dodavanje/Uklanjanje uloge
class ChangeRoles(ListAPIView):
    queryset = user.objects.all()
    serializer_class = UserRoleSerializer
    detail_serializer_class = UserClientRenterSerializer

    def post(self, request, format=None):
        serializer = UserRoleSerializer(data=request.data)
        if serializer.is_valid():
            User = user.objects.get(email=serializer.data['email'])
            User.isClient = serializer.data['isClient']
            User.isRenter = serializer.data['isRenter']
            User.save()
            serializer = UserClientRenterSerializer(User)
            return Response({'user': serializer.data})
        else:
            return Response(serializer.errors, status=status.HTTP_404_NOT_FOUND)


# UC11 - Pregled profila korisnika
class PublicData(ListAPIView):
    queryset = user.objects.all()
    serializer_class = EmailSerializer

    def post(self, request, format=None):
        serializer = EmailSerializer(data=request.data)
        if serializer.is_valid():
            if user.objects.filter(email=serializer.data['email']):
                currentUser = user.objects.filter(email=serializer.validated_data['email'])[0]
                currentshowUserData = showUserData.objects.filter(userId=currentUser)[0]
                if (currentshowUserData.showName == False):
                    currentUser.name = None
                if (currentshowUserData.showSurname == False):
                    currentUser.surname = None
                if (currentshowUserData.showEmail == False):
                    currentUser.email = None
                serializer1 = PublicDataSerializer(currentUser)

                currentReceivedRatingQuerySet = rating.objects.filter(clientId=currentUser)
                currentGivenRatingQuerySet = rating.objects.filter(renterId=currentUser)
                tempListClient = []
                for curr in currentReceivedRatingQuerySet:
                    serializer2 = PublicDataSerializer2(curr)
                    tempListClient.append(serializer2.data)
                tempListRenter = []
                for curr in currentGivenRatingQuerySet:
                    serializer3 = PublicDataSerializer3(curr)
                    tempListRenter.append(serializer3.data)
                return Response({'user': serializer1.data, 'ratingsReceivedAsClient': tempListClient,
                                 'ratingsGivenAsRenter': tempListRenter}, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_404_NOT_FOUND)


# UC12 - Registracija romobila
class CreateScooter(ListAPIView):
    queryset = scooter.objects.all()
    serializer_class = CreateScooterSerializer

    def post(self, request, format=None):
        serializer = CreateScooterSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({'scooter': serializer.data}, status=status.HTTP_201_CREATED)
        if serializer.is_valid():
            pass
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# UC13 - Brisanje romobila
class DeleteScooter(APIView):
    queryset = scooter.objects.all()
    serializer_class = DeleteScooterSerializer

    def post(self, request, format=None):
        serializer = DeleteScooterSerializer(data=request.data)
        if serializer.is_valid():
            if scooter.objects.filter(id=serializer.data['id']):
                scooter.objects.filter(id=serializer.data['id']).delete()
                return Response(status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_404_NOT_FOUND)

    '''def get_object(self, pk):
        try:
            return scooter.objects.get(pk=pk)
        except scooter.DoesNotExist:
            raise Http404

    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_200_OK)

    queryset = scooter.objects.all()
    serializer_class = ScooterIdSerializer

    def post(self, request, format=None):
        serializer = ScooterIdSerializer(data=request.data)
        if serializer.is_valid():
            if scooter.objects.filter(id=serializer.data['id']):
                scooter.objects.filter(id=serializer.data['id']).delete()
                return Response(status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_404_NOT_FOUND)'''


# UC14 - Pregled popisa vlastitih romobila
class GetScooterList(ListAPIView):
    queryset = user.objects.all()
    serializer_class = EmailSerializer

    def post(self, request, format=None):
        serializer = EmailSerializer(data=request.data)
        scooter_serializer = []
        if serializer.is_valid():
            if user.objects.filter(email=serializer.data['email']):
                current_user = user.objects.get(email=serializer.data['email'])
                scooterList = scooter.objects.filter(renterId=current_user.id)

                for sc in scooterList:
                    temp = GetScooterSerializer(sc)
                    scooter_serializer.append(temp.data)
                    '''scooter_photos = ['scooterPhotos: ']
                    photoList = scooterPhoto.objects.filter(scooterId=sc.id)
                    scooter_photos2 = []
                    for ph in photoList:
                        scooter_photos2.append(ph.imgURL)
                    scooter_photos.append(scooter_photos2)
                    scooter_serializer.append(scooter_photos)'''
                return Response({'scooters': scooter_serializer}, status=status.HTTP_200_OK)
            return Response(serializer.errors, status=status.HTTP_404_NOT_FOUND)
        return Response(serializer.errors, status=status.HTTP_404_NOT_FOUND)


# UC15 - Dodavanje slika romobila:
class AddScooterPhoto(ListAPIView):
    queryset = scooterPhoto.objects.all()
    serializer_class = AddScooterPhotoSerializer

    def post(self, request, format=None):
        serializer = AddScooterPhotoSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({'scooter': serializer.data}, status=status.HTTP_201_CREATED)
        if serializer.is_valid():
            pass
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# UC16 - Brisanje slike romobila
class DeleteScooterPhoto(APIView):
    queryset = scooterPhoto.objects.all()
    serializer_class = DeleteScooterPhotoSerializer

    def post(self, request, format=None):
        serializer = DeleteScooterPhotoSerializer(data=request.data)
        if serializer.is_valid():
            if scooterPhoto.objects.filter(id=serializer.data['id']):
                scooterPhoto.objects.filter(id=serializer.data['id']).delete()
                return Response(status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_404_NOT_FOUND)

    '''def get_object(self, pk):
        try:
            return scooterPhoto.objects.get(pk=pk)
        except scooterPhoto.DoesNotExist:
            raise Http404

    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_200_OK)'''


# UC17 - Dohvacanje liste vlastitih ponuda
class GetRenterOffers(APIView):
    queryset = user.objects.all()
    serializer_class = EmailSerializer

    def post(self, request, format=None):
        serializer = EmailSerializer(data=request.data)
        if serializer.is_valid():
            tech = user.objects.filter(email=serializer.data['email'])[0]
            ispis = []
            scList = scooter.objects.filter(renterId=tech.id)
            for sc in scList:
                offerList = rentOffer.objects.filter(scooterId=sc.id)
                for of in offerList:
                    serializer2 = GetRentOfferSerializer(of)
                    ispis.append(serializer2.data)
            return Response({'rentOffer': ispis}, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class GetRenterOffersClient(APIView):
    queryset = user.objects.all()
    serializer_class = EmailSerializer

    def post(self, request, format=None):
        serializer = EmailSerializer(data=request.data)
        if serializer.is_valid():
            tech = user.objects.get(email=serializer.data['email'])
            ispis = []
            roList = rentOffer.objects.filter(clientId=tech.id)
            for ro in roList:
                serializer2 = GetRentOfferSerializer(ro)
                ispis.append(serializer2.data)
            return Response({'rentOffer': ispis}, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# UC 18 - objava ponude
class CreateRentOffer(ListAPIView):
    queryset = rentOffer.objects.all()
    serializer_class = CreateRentOfferSerializer

    def post(self, request, format=None):
        serializer = CreateRentOfferSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({'rentOffer': serializer.data}, status=status.HTTP_201_CREATED)
        if serializer.is_valid():
            pass
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# UC 19 - pregled zahtjeva klijenata za ponudu
class GetClientRequests(ListAPIView):
    queryset = rentOffer.objects.all()
    serializer_class = RentOfferIdSerializer

    def post(self, request, format=None):
        serializer = RentOfferIdSerializer(data=request.data)
        if serializer.is_valid():
            if chat.objects.filter(rentOfferId=serializer.data['id']):
                reqlist = chat.objects.filter(rentOfferId=serializer.data['id'])
                ispis = []
                for r in reqlist:
                    serializer2 = ClientRequestSerializer(r)

                    ispis.append(serializer2.data)
                return Response({'clients': ispis}, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# UC 20 Podvrda zahtjea klijenta za ponudu
class ConfirmClientRequestForOffer(ListAPIView):
    queryset = chat.objects.all()
    serializer_class = ConfirmClientRequestForOfferSerializer

    def post(self, request, format=None):
        serializer = ConfirmClientRequestForOfferSerializer(data=request.data)
        if serializer.is_valid():
            if rentOffer.objects.filter(id=serializer.data['rentOfferId']) and chat.objects.filter(
                    pk=serializer.data['chatId'], rentOfferId=serializer.data['rentOfferId']):
                ch = chat.objects.get(pk=serializer.data['chatId'])
                offe = rentOffer.objects.get(pk=serializer.data['rentOfferId'])
                ofList = chat.objects.filter(rentOfferId=offe)
                ispis = []
                for of in ofList:
                    messageNo = message.objects.get(chatId=of, messageNumber=1)
                    messageNo.acceptedRequest = False
                    messageNo.time = datetime.datetime.now()
                    messageNo.save()
                messageYes = message.objects.get(chatId=ch, messageNumber=1)
                messageYes.acceptedRequest = True
                messageYes.time = datetime.datetime.now()
                offe.clientId = ch.clientId
                offe.save()
                messageYes.save()

                # pocetak
                messages = message.objects.filter(chatId_id=serializer.data['chatId']).values_list(
                    'messageNumber').order_by(
                    'messageNumber').reverse()
                lastMessageNumberId = messages[0][0]
                newMessageId = lastMessageNumberId + 1

                newMessageObject = message(chatId=ch, messageNumber=newMessageId,
                                           text="Request accepted.",
                                           time=datetime.datetime.now())
                newMessageObject.save()

                returningDict = {
                    "chatId": serializer.data['chatId'],
                    "messageNumber": newMessageId,
                    "text": "Request accepted.",
                    "time": datetime.datetime.now()
                }

                # kraj
                return Response({'Message': returningDict}, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# UC Bezimeni, Dohvati prijave klijenata
class GetClientReports(APIView):
    queryset = user.objects.all()
    serializer_class = EmailSerializer

    def post(self, request, format=None):
        serializer = EmailSerializer(data=request.data)
        ispis = []
        if serializer.is_valid():
            us = user.objects.get(email=serializer.data['email'])
            scotList = scooter.objects.filter(renterId=us)
            for sco in scotList:
                renList = rentOffer.objects.filter(scooterId=sco)
                for re in renList:
                    repList = reportPhoto.objects.filter(rentOfferId=re)
                    for r in repList:
                        serializer2 = ReportPhotoSerializer2(r)
                        ispis.append(serializer2.data)
            return Response({'clientReports': ispis}, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_404_NOT_FOUND)


# UC 21 Odogvor na prijavu klijenta
class AnswerClientReport(APIView):
    queryset = reportPhoto.objects.all()
    serializer_class = AnswerReportPhotoSerializer

    def post(self, request, format=None):

        serializer = AnswerReportPhotoSerializer(data=request.data)
        if serializer.is_valid():
            rep = reportPhoto.objects.get(id=serializer.data['id'])
            rep.isAccepted = serializer.data['isAccepted']
            if rep.isAccepted is True:
                tech = rentOffer.objects.get(id=rep.rentOfferId.id)
                scoot = scooter.objects.get(id=tech.scooterId.id)
                photlist = scooterPhoto.objects.filter(scooterId=scoot)
                #print('**************', photlist, scoot)
                for ph in photlist:
                    ph.delete()
                    #print('**************',ph)
                toadd=reportedPhotoAdd.objects.filter(reportPhotoId=rep.id)
                for ad in toadd:
                    u = scooterPhoto(scooterId=scoot, imgURL=ad)
                    #print(u, u.scooterId, u.imgURL)
                    u.save()
            rep.save()
            serializer2 = AnswerReportPhotoSerializer2(rep).data
            if (rep.isAccepted == False):
                serializer2 = None
            ispis = [datetime.datetime.now(), serializer2]
            return Response({'reportPhoto': ispis}, status=status.HTTP_200_OK)


# UC 22 Slanje poruke u razgovoru
class SendMessage(ListAPIView):
    queryset = chat.objects.all()
    serializer_class = SendMessageSerializer

    def post(self, request, format=None):

        serializer = SendMessageSerializer(data=request.data)
        if serializer.is_valid():
            if (chat.objects.filter(id=serializer.data['id'])):
                try:
                    row = chat.objects.get(id=serializer.data['id'])  # .values('id')
                    text = serializer.data['text']
                    gettingId = chat.objects.filter(id=serializer.data['id']).values('id')
                    chatId = gettingId[0]['id']
                    messages = message.objects.filter(chatId_id=chatId).values_list('messageNumber').order_by(
                        'messageNumber').reverse()
                    lastMessageNumberId = messages[0][0]
                    newMessageId = lastMessageNumberId + 1
                    newMessageObject = message(chatId=row, messageNumber=newMessageId, text=text,
                                               time=datetime.datetime.now())
                    newMessageObject.save()

                    # pocetak

                    returningDict = {
                        "chatId": serializer.data['id'],
                        "messageNumber": newMessageId,
                        "text": text,
                        "time": datetime.datetime.now()
                    }

                    # kraj

                    # serializer2 = TimestampSerializer(newMessageObject)
                    return Response({'Message': returningDict}, status=status.HTTP_200_OK)
                except:
                    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# UC 23 Pregled razgovora - vracam listu id-a chatova
#                         - ako ne postoje razgovori bacam HTTP_400_BAD_REQUEST
class ShowChats(ListAPIView):
    queryset = user.objects.all()
    serializer_class = EmailSerializer

    def post(self, request, format=None):

        serializer = EmailSerializer(data=request.data)
        if serializer.is_valid():
            loadedId = user.objects.get(email=serializer.data['email']).id
            chats = chat.objects.filter(Q(clientId=loadedId) | Q(renterId=loadedId))
            ispis = []
            for ch in chats:
                listElement = []
                if ch.clientId.id == loadedId:
                    # onda je renter
                    nickname = user.objects.get(pk=ch.renterId.id).nickname

                else:
                    # onda je client
                    nickname = user.objects.get(pk=ch.clientId.id).nickname

                listElement.append(nickname)
                listElement.append(ch.id)

                rentOfferObject = rentOffer.objects.get(id=ch.rentOfferId.id)
                scooterObject = scooter.objects.get(id=rentOfferObject.id)

                listElement.append(CreateRentOfferSerializer(rentOfferObject).data)
                listElement.append(ScooterInfoForChatSerializer(scooterObject).data)

                photoList = scooterPhoto.objects.filter(scooterId=scooterObject.id)
                photoElements = []
                for photoElement in photoList:
                    photoElements.append(photoElement.imgURL)

                listElement.append(photoElements)
                ispis.append(listElement)
            return Response({'chats': ispis}, status=status.HTTP_200_OK)

        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# UC 24 pregled poruka u razgovoru - osim same poruke vracam i redni broj poruke
#                                  - ako ne postoje poruke bacam HTTP_400_BAD_REQUEST
class ShowMessages(ListAPIView):
    queryset = chat.objects.all()
    serializer_class = ChatIdSerializer

    def post(self, request, format=None):

        serializer = ChatIdSerializer(data=request.data)
        if serializer.is_valid():
            try:
                chats = chat.objects.filter(id=serializer.data['id']).values('id')
                messages = message.objects.filter(chatId_id=chats[0]['id']).values('messageNumber', 'text')
                if messages:
                    return Response({'messages': messages}, status=status.HTTP_200_OK)
                else:
                    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
            except:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# UC 25 Iniciranje provodenja transakcije - "get_transaction/"
class CreateTransaction(ListAPIView):
    queryset = rentOffer.objects.all()
    serializer_class = CreateTransactionSerializer

    def post(self, request, format=None):
        serializer = CreateTransactionSerializer(data=request.data)  # prima id rentOffera
        if serializer.is_valid():

            rentOfferObject = rentOffer.objects.filter(id=serializer.data['id']).values('priceKm')
            price = rentOfferObject[0]['priceKm']

            rentOfferObject = rentOffer.objects.filter(id=serializer.data['id']).values('returnTime')
            time = rentOfferObject[0]['returnTime']
            time = time.replace(tzinfo=None)
            if time < datetime.datetime.now():
                penalty = True
                rentOfferObject = rentOffer.objects.filter(id=serializer.data['id']).values('returnPenalty')
                price = price + rentOfferObject[0]['returnPenalty']
            else:
                penalty = False

            distanceCovered = random.uniform(3.0, 10.0)
            price = price * decimal.Decimal(distanceCovered)

            returningDict = {
                "distanceCovered": distanceCovered,
                "price": price,
                "penalty": penalty,
                "time": time
            }
            return Response({'transaction': returningDict}, status=status.HTTP_201_CREATED)
        return Response(status=status.HTTP_400_BAD_REQUEST)


# UC 25 Iniciranje provodenja transakcije - "confirm_transaction/"
class ConfirmTransaction(ListAPIView):
    queryset = transaction.objects.all()
    serializer_class = ConfirmTransactionSerializer

    def post(self, request, format=None):
        serializer = ConfirmTransactionSerializer(data=request.data)
        if serializer.is_valid():
            distanceCovered = serializer.data['distanceCovered']
            price = serializer.data['price']
            penalty = serializer.data['pentalty']
            time = serializer.data['time']

            rentOfferObject = rentOffer.objects.filter(returnTime=time).values('id', 'clientId_id')
            rentOfferId = rentOfferObject[0]['id']
            rentOfferClientId = rentOfferObject[0]['clientId_id']

            ratingObject = rating.objects.filter(clientId_id=rentOfferClientId).values('id')
            ratingId = ratingObject[0]['id']

            chatObjects = chat.objects.filter(rentOfferId=rentOfferId, clientId_id=rentOfferClientId).values('id')
            chatId = chatObjects[0]['id']

            messages = message.objects.filter(chatId_id=chatId).values_list('messageNumber').order_by(
                'messageNumber').reverse()
            lastMessageNumberId = messages[0][0]
            newMessageId = lastMessageNumberId + 1

            tran = transaction(distanceCovered=distanceCovered, price=price, pentalty=penalty, time=time,
                               ratingId_id=ratingId, rentOfferId_id=rentOfferClientId)
            tran.save()

            chatObject = chat.objects.get(id=chatId)
            currentTime = datetime.datetime.now()
            newMessageObject = message(chatId=chatObject, messageNumber=newMessageId, text="Transaction successful.",
                                       time=currentTime)
            newMessageObject.save()
            # serializer2 = TimestampSerializer(newMessageObject)
            returningDict = {
                "chatId": chatId,
                "messageNumber": newMessageId,
                "text": "Transaction successful.",
                "time": currentTime
            }
            return Response({'Message': returningDict}, status=status.HTTP_200_OK)

        return Response(status=status.HTTP_400_BAD_REQUEST)


# UC 26 Ocjenjivanje klijenta
class CreateRating(ListAPIView):
    queryset = rating.objects.all()
    serializer_class = CreateRatingSerializer

    def post(self, request, format=None):
        serializer = CreateRatingSerializer(data=request.data)
        if serializer.is_valid():

            rentOfferId = serializer.data['rentOfferId']

            loadedRating = serializer.data['rating']
            if loadedRating not in [1, 2, 3, 4, 5]: return Response(serializer.errors,
                                                                    status=status.HTTP_400_BAD_REQUEST)

            comment = serializer.data['comment']

            rentOfferObject = rentOffer.objects.filter(id=rentOfferId).values('clientId_id')
            clientId = rentOfferObject[0]['clientId_id']

            try:
                chatObject = chat.objects.filter(rentOfferId_id=rentOfferId, clientId_id=clientId).values('renterId_id')
                renterId = chatObject[0]['renterId_id']
                rtng = rating(rating=loadedRating, comment=comment, clientId_id=clientId, renterId_id=renterId)
                rtng.save()
                return Response(status=status.HTTP_201_CREATED)
            except:
                return Response(serializer.errors, status.HTTP_400_BAD_REQUEST)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# UC 27 Slanje zahtjeva za najam romobila
class SendRequestForOffer(APIView):
    queryset = rentOffer.objects.all()
    serializer_class = SendRequestForOfferSerializer

    def post(self, request, format=None):

        serializer = SendRequestForOfferSerializer(data=request.data)
        if serializer.is_valid():
            tech = rentOffer.objects.get(pk=serializer.data['id'])
            if user.objects.filter(id=serializer.data['senderId']):
                senid = user.objects.get(pk=serializer.data['senderId'])
            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
            text = serializer.data['text']
            if text == "":
                text = "Pozdrav, želio bih unajmiti tvoj romobil."
            sco = scooter.objects.get(pk=tech.scooterId.id)
            renid = sco.renterId
            ch = chat(rentOfferId=tech, clientId=senid, renterId=renid)
            if (chat.objects.filter(rentOfferId=ch.rentOfferId, clientId=ch.clientId, renterId=ch.renterId)):
                pass
            else:
                ch.save()
                msg = message(chatId=ch, messageNumber=1, text=text, time=datetime.datetime.now())
                msg.save()
                serializer2 = TimestampSerializer(msg)
                return Response({'text': text, 'Timestamp': serializer2.data}, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# UC 28 Slanje prijave slika
class SendReportForPhoto(APIView):
    queryset = reportPhoto.objects.all()
    serializer_class = ReportPhotoSerializer

    def post(self, request, format=None):
        serializer = ReportPhotoSerializer(data=request.data)
        if serializer.is_valid():
            ro = rentOffer.objects.get(id=serializer.data['rentOfferId'])
            tempstring = serializer.data['photosToAdd']
            photosAddList = tempstring.split(",")
            tempstring = serializer.data['photosToRemove']
            photosRemoveList = tempstring.split(",")
            new = reportPhoto(rentOfferId=ro, cause=serializer.data['cause'])
            new.save()
            for pa in photosAddList:
                # print(new,pa)
                reportedPhotoAdd.objects.create(reportPhotoId=new, newPhotoULR=pa)
                # newA=reportedPhotoAdd(reportPhotoId=new, newPhotoULR=pa)
                # newA.save()
            for pr in photosRemoveList:
                # print(new,pr)
                reportedPhotoRemove.objects.create(reportPhotoId=new, oldPhotoULR=pr)
                # newR=reportedPhotoRemove(reportPhotoId=new, oldPhotoULR=pr)
                # newR.save()
            return Response({'cause': serializer.data['cause'], 'Timestamp': datetime.datetime.now()},
                            status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
