from django.db import models


class user(models.Model):
#    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=30)  # mozda treba: blank=False, default=None, a mozda i ne treba
    surname = models.CharField(max_length=30)
    nickname = models.CharField(max_length=30)
    creditCardNumber = models.CharField(max_length=30)
    email = models.CharField(max_length=30)
    password = models.CharField(max_length=30)
    idURL = models.TextField(null=True)
    recordURL = models.TextField(null=True)
    isActive = models.BooleanField(null=True)
    isClient = models.BooleanField(default=False)
    isRenter = models.BooleanField(default=False)

    def _is_active(self):
        return self.isActive


#    def __str__(self):
#       return str(self.id)
'''
class role(models.Model):
    userId = models.ForeignKey(user, on_delete=models.CASCADE, primary_key=True)
    isClient = models.BooleanField()
    isRenter = models.BooleanField()
    isAdmin = models.BooleanField()


#    def __str__(self):
#       return str(self.id)
'''


class showUserData(models.Model):
    userId = models.ForeignKey(user, on_delete=models.CASCADE)
    showName = models.BooleanField(default=True)
    showSurname = models.BooleanField(default=True)
    showNickname = models.BooleanField(default=True)
    showEmail = models.BooleanField(default=True)


#    def __str__(self):
#       return str(self.id)

class rating(models.Model):
    clientId = models.ForeignKey(user, on_delete=models.CASCADE, related_name='client')
    renterId = models.ForeignKey(user, on_delete=models.CASCADE, related_name='renter')
    rating = models.IntegerField()
    comment = models.CharField(max_length=1000)#, null=True)


#    def __str__(self):
#       return str(self.id)

class scooter(models.Model):
    renterId = models.ForeignKey(user, on_delete=models.CASCADE)
    comment = models.CharField(max_length=1000)
    isAvailable = models.BooleanField()

    def __str__(self):
        return str(self.id)


class scooterPhoto(models.Model):
    scooterId = models.ForeignKey(scooter, on_delete=models.CASCADE)
    imgURL = models.TextField()

    class Meta:
        unique_together = (('scooterId', 'imgURL'),)


#    def __str__(self):
#       return str(self.id)

class rentOffer(models.Model):
    scooterId = models.ForeignKey(scooter, on_delete=models.CASCADE)
    clientId = models.ForeignKey(user, on_delete=models.CASCADE, default=None, null=True, blank=True)
    currentLocationLatitude = models.DecimalField(max_digits=7, decimal_places=5)
    currentLocationLongitude = models.DecimalField(max_digits=7, decimal_places=5)
    returnLocationLatitude = models.DecimalField(max_digits=7, decimal_places=5)
    returnLocationLongitude = models.DecimalField(max_digits=7, decimal_places=5)
    returnTime = models.DateTimeField()  # Mozda ce smetati
    priceKm = models.DecimalField(max_digits=10, decimal_places=2)
    returnPenalty = models.DecimalField(max_digits=10, decimal_places=2)


#    def __str__(self):
#       return str(self.id)

class chat(models.Model):
    rentOfferId = models.ForeignKey(rentOffer, on_delete=models.CASCADE)
    clientId = models.ForeignKey(user, on_delete=models.CASCADE, related_name='clientId')
    renterId = models.ForeignKey(user, on_delete=models.CASCADE, related_name='renterId')

    # nekako napraviti UNIQUE, brisu li se razgovori ako se obrisje korisnik?*******************
    # mozda ovdje treba alternativni kljic


#    def __str__(self):
#       return str(self.id)

class message(models.Model):
    chatId = models.ForeignKey(chat, on_delete=models.CASCADE)#, primary_key=True)
    messageNumber = models.IntegerField()
    text = models.CharField(max_length=1000)
    time = models.DateTimeField(auto_now_add=True)
    acceptedRequest = models.BooleanField(default=None, null=True)

    class Meta:
        unique_together = (('chatId', 'messageNumber'),)


#    def __str__(self):
#       return str(self.id)

class transaction(models.Model):
    rentOfferId = models.ForeignKey(rentOffer, on_delete=models.CASCADE)
    distanceCovered = models.DecimalField(max_digits=5, decimal_places=2)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    pentalty = models.BooleanField()
    time = models.DateTimeField(auto_now_add=True)
    ratingId = models.ForeignKey(rating, on_delete=models.CASCADE)


#    def __str__(self):
#       return str(self.id)

class reportPhoto(models.Model):
    rentOfferId = models.ForeignKey(rentOffer, on_delete=models.CASCADE)
    cause = models.CharField(max_length=1000)
    isAccepted = models.BooleanField(default=None, null=True)
    isAcceptedByAdmin = models.BooleanField(default=None, null=True)


#    def __str__(self):
#       return str(self.id)

class reportedPhotoRemove(models.Model):
    reportPhotoId = models.ForeignKey(reportPhoto, on_delete=models.CASCADE)#, primary_key=True
    oldPhotoULR = models.TextField()

    class Meta:
        unique_together = (('reportPhotoId', 'oldPhotoULR'),)
        '''unique_together = ('reportPhotoId', 'oldPhotoULR')
        unique_together = (('reportPhotoId', 'oldPhotoULR'),)
        constraints = [
            models.UniqueConstraint(fields=['reportPhotoId', 'oldPhotoULR'], name='reportedPhotoRemoveConstraint')
        ]'''

#    def __str__(self):
#       return str(self.id)

class reportedPhotoAdd(models.Model):
    reportPhotoId = models.ForeignKey(reportPhoto, on_delete=models.CASCADE)#, primary_key=True
    newPhotoULR = models.TextField()

    class Meta:
        unique_together = (('reportPhotoId', 'newPhotoULR'),)
        '''unique_together = ('reportPhotoId', 'newPhotoULR')
        'unique_together = (('reportPhotoId', 'newPhotoULR'),)
        constraints = [
            models.UniqueConstraint(fields=['reportPhotoId', 'newPhotoULR'], name='reportedPhotoAddConstraint')
        ]'''

#    def __str__(self):
#       return str(self.id)
